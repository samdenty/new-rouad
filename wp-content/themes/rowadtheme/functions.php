<?php
/**
 * ROWAD PLATFORM functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ROWAD_PLATFORM
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rowadtheme_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on ROWAD PLATFORM, use a find and replace
		* to change 'rowadtheme' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'rowadtheme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'rowadtheme' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'rowadtheme_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'rowadtheme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function rowadtheme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rowadtheme_content_width', 640 );
}
add_action( 'after_setup_theme', 'rowadtheme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rowadtheme_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'rowadtheme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'rowadtheme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'rowadtheme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function rowadtheme_scripts() {
	wp_enqueue_style( 'rowadtheme-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'rowadtheme-style', 'rtl', 'replace' );

	wp_enqueue_script( 'rowadtheme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'rowadtheme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}
function rowad_coming(){
	if (!is_user_logged_in()){
		require_once 'comingsoon/index.php';
		die();
	}
	
}
add_action('get_header', 'rowad_coming');

// hide update notifications
function remove_core_updates(){
    global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates');
function run_activate_plugin( $plugin ) {
    $current = get_option( 'active_plugins' );
    $plugin = plugin_basename( trim( $plugin ) );

    if ( !in_array( $plugin, $current ) ) {
        $current[] = $plugin;
        sort( $current );
        do_action( 'activate_plugin', trim( $plugin ) );
        update_option( 'active_plugins', $current );
        do_action( 'activate_' . trim( $plugin ) );
        do_action( 'activated_plugin', trim( $plugin) );
    }

    return null;
}
run_activate_plugin( 'woocommerce/woocommerce.php' );

add_action( 'admin_bar_menu', 'modify_admin_bar' );

function modify_admin_bar( $wp_admin_bar ){
    global $wp_admin_bar;
 //   var_dump($wp_admin_bar);
   // add_menu_page($wp_admin_bar[0]);
//    $wp_admin_bar->add_menu( array(
//        'id' => '1233', // an unique id (required)
//        'parent' => false, // false for a top level menu
//        'title' => 'qwewqe', // title/menu text to display
//        'href' => 'qweqew', // target url of this menu item
//        // optional meta array
//        'meta' => array(
//            'onclick' => 'qwew',
//            'html' => 'qweqwe',
//            'class' => 'we',
//            'target' => 'qweqe',
//        )
//    ) );

    // do something with $wp_admin_bar;
//var_dump($wp_admin_bar);
}

add_action( 'wp_before_admin_bar_render', 'remove_handler' );
function remove_handler()
{
    echo "<style>
#wpadminbar {

	min-width: auto !important;
	width: fit-content !important;
}
</style>";
    global $wp_admin_bar;
    $menu_id = 'new-content'; // the menu id which you want to remove
   $wp_admin_bar->remove_menu($menu_id);
    $menu_id = 'wp-logo'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
  $menu_id = 'comments'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
    $menu_id = 'site-name'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
    $args = array(
        'id' => 'my-account', // id of the existing child node
        'parent' => false // make it a top level node
    );

    $wp_admin_bar->add_node($args);
}

function rouad_admin_color_scheme() {
    //Get the theme directory
    $theme_dir = get_stylesheet_directory_uri();

    //rouad
    wp_admin_css_color( 'rouad', __( 'rouad' ),
        $theme_dir . '/rouad.css',
        array( ' #59524c', '#fff', '#9ea476' , '#c7a589')
    );
}
add_action('admin_init', 'rouad_admin_color_scheme');
function admin_color_scheme() {
    global $_wp_admin_css_colors;
   //var_dump(json_encode($_wp_admin_css_colors));
    $_wp_admin_css_colors = array();
    $theme_dir = get_stylesheet_directory_uri();
    get_current_screen()->remove_help_tabs();
    get_current_screen()->remove_options();
    get_current_screen()->remove_option("metabox-prefs");
    //rouad
    wp_admin_css_color( 'rouad', __( 'rouad' ),
        $theme_dir . '/rouad.css',
        array( ' #59524c', '#fff', '#9ea476' , '#c7a589')
    );
}
add_action('admin_head', 'admin_color_scheme');

add_action( 'admin_head-profile.php', 'wpse_72463738_remove_admin_color_scheme_picker' );

/**
 * Remove the color picker from the user profile admin page.
 */
function wpse_72463738_remove_admin_color_scheme_picker() {
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
}
add_filter( 'screen_options_show_screen', 'wpster_remove_screen_options', 10, 2 );
function wpster_remove_screen_options( $display_boolean, $wp_screen_object ){


return false;
}



function change_post_menu_label() {
    global $menu;
    global $submenu;
   // $menu[2][0] = 'dashboard';
    //var_dump($menu);

    echo '';
}

function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['dashboard']->labels;
        $labels->name = 'Contacts';
        $labels->singular_name = 'Contact';
        $labels->add_new = 'Add Contact';
        $labels->add_new_item = 'Add Contact';
        $labels->edit_item = 'Edit Contacts';
        $labels->new_item = 'Contact';
        $labels->view_item = 'View Contact';
        $labels->search_items = 'Search Contacts';
        $labels->not_found = 'No Contacts found';
        $labels->not_found_in_trash = 'No Contacts found in Trash';
    }
   // add_action( 'init', 'change_post_object_label' );
    add_action( 'admin_menu', 'change_post_menu_label' );

