<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
<section class="first-section">
    <div class="text-section">
        <div class="title-text-section">
            <?= __("من نحن") ?>

        </div>
        <div class="description">
            <?= __("منصة روّاد هي شركة إماراتية رائدة في مجال التجارة الإلكترونية
تقدم خدماتها في دولة الإمارات العربية المتحدة لكافة الشركات
والأنشطة التجارية
نسعى من خلال المتاجر الإلكترونية التي نقدمها لزبائننا بآلية جديدة
ومبتكرة أن نخلق مجتمعاً جديداً لرواد الأعمال
منذ البدء في أعمالنا وضعنا أهدافاً تتمثل في إعطاء الفرصة لذوي
النشاطات التجارية الناشئة للوصول إلى بوابة التجارة الإلكترونية
وتسديد التكاليف المترتبة عليها من أرباحهم") ?>
        </div>
    </div>
    <div class="photo-section">
        <img src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-2-1-881x1024.png">
    </div>
</section>
<section class="second-section">

    <div class="photo-section">
        <img src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-2-2-992x1024.png">
    </div>
    <div class="text-section">
        <div class="title-text-section">
            <?= __("فريق منصّة روّاد ") ?>

        </div>

        <div class="description">
            <?= __("لدينا فريق عربي شغوف ومتميز يعمل على إنجاز المهام بطريقة
إبداعية
منذ تأسيس منصّة روّاد بدأنا بإعداد فريق متخصص من المهندسين
والتقنيين للوصول إلى جودة عالية في الخدمات المتعلقة بالمتاجر
الإلكترونية.
ابتداءً بالتأسيس والبرمجة ،مروراً بالاختبار وحتى الوصول إلى خدمات
مابعد البيع.") ?>
        </div>
    </div>
</section>
<style>

    .photo-section, .text-section {
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }

    .first-section, .second-section {
        display: flex;
        font-size: 18px;
        justify-content: center;
        align-items: center;
    }

    .site-content .ast-container {
        display: block;
    }

    .photo-section img,.description {
        max-width: 450px;
        width:100%;
    }

    @media only screen and (min-width: 776px) {
        .text-section {
            width: 50%;
        }

        .photo-section {
            width: 50%;
        }
    }


    @media only screen and (max-width: 992px) {
        .first-section,.second-section{
         margin:30px !important;
        }
    }
    @media only screen and (max-width: 776px) {
        .first-section {
            flex-direction: column-reverse;

        }

        .second-section {
            flex-direction: column;

        }
    }

    .title-text-section {
        color: #1152f2;
        font-size: 30px;
        width: 100%;
        max-width: 450px;
        font-weight: bolder;
        margin: auto;
        text-align: right;

    }
</style>
<?php get_footer(); ?>
