<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
//var_dump();
$url = explode("/", $_SERVER['REQUEST_URI']);
if ($url[1] == "%D9%85%D9%86-%D9%86%D8%AD%D9%86") {
    $activ_class = "who-line";
    $active_mobile = "who-us";
} else
    if ($url[1] == "") {
        $activ_class = "main-line";
        $active_mobile = "main-site";
    }
$url = explode("/", $_SERVER['REQUEST_URI']);
?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <style>

        .<?=$active_mobile ?> {
            color: #FFFFFF !important;
            background-color: #1152f2 !important;

        }

        .<?=$activ_class ?> {
            border-bottom: 4px solid #1152f2;
        }
    </style>
    <?php astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">


    <?php wp_head(); ?>
    <?php astra_head_bottom(); ?>
    <style>
        section.main-header,section.first-section,section.second-section,section.fo-section,section.fi-section,section.go-section,section.seven-section{
            max-width:1190px;
            margin :auto;
        }
        @font-face {
            font-family: "URW DIN Arabic";
            src: "<?=site_url() ?>/wp-content/uploads/2023/01/font/FontsFree-Net-URW-DIN-Arabic-Bold.ttf");
        }

        @font-face {
            font-family: "font22" ;
            src: "<?=site_url() ?>/wp-content/uploads/2023/01/font/FontsFree-Net-URW-DIN-Arabic-Bold.ttf";
        }


        @font-face {
            font-family: myFirstFont;
            src: url(Medium.ttf);
            src: url(URWDINArabic-Regular.woff)
        ;
        }


        [class*=" eicon-"], [class^=eicon] {
            display: inline-block;
            font-family: eicons !important;
        }

        .fa, .fas {
            font-family: "Font Awesome 5 Free" !important;
            font-weight: 900;
        }

        .fa:after, .fas:before {
            font-family: "Font Awesome 5 Free" !important;
            font-weight: 900;
        }

        .fab:after, .fab:before {
            font-family: "Font Awesome 5 Brands" !important;
            font-weight: 400;
        }

        .rtl #wpadminbar * {
            font-family: Tahoma, sans-serif !important;
        }


        .who-us {
            color: #1152f2;
            background-color: #FFFFFF;
        }

    </style>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<div
    <?php
    echo astra_attr(
        'site',
        array(
            'id' => 'page',
            'class' => 'hfeed site',
        )
    );

    ?>>
    <style>
        .mobile-open-menu div {
            margin: 10px;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            font-size: 16px;
            border: 1px solid;
            color: #1152f2;

        }

        .mobile-open-menu {
            display: none;
            position: fixed;
            background-color: #FFFFFF;
            width: 100%;
            top: 0px;
            padding-top:110px;
            z-index: 100;
        }
    </style>
    <div class="menu-mobile">
        <div class="mobile-open-menu">
            <a href="<?= site_url() ?> ">
                <div class="main-site">
                    الرئيسية
                </div>
            </a>
            <a href="<?= site_url() ?>/من-نحن/">
                <div class="who-us">
                    من نحن
                </div>
            </a>

            <a href="<?= site_url() ?>/#backeg ">
                <div class="packages">
                    الباقات
                </div>
            </a>

        </div>

    </div>
    <a class="skip-link screen-reader-text"
       href="#content"><?php echo esc_html(astra_default_strings('string-header-skip-link', false)); ?></a>
    <div class="site-header">
        <section class="main-header">
            <div class="menu-icon menu-open">
                <img class="menu-open-icon" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/open-menu.svg" alt="logo">
                <img class="menu-open-icon-w" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/open-menu-w.svg"
                     alt="logo">
            </div>
            <div class="menu-icon menu-close">
                <img class="menu-close-icon" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/close-menu.svg"
                     alt="logo">
                <img class="menu-close-icon-w" src="<?= site_url() ?>/wp-content/uploads/2023/01/nav/close-menu-w.svg"
                     alt="logo">
            </div>
            <div class="rouad-logo rouad-logo-b">
                <a href="<?= site_url() ?> ">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/1-300x97.png " alt="logo">
                </a>
            </div>
            <div class="rouad-logo rouad-logo-w">
                <a href="<?= site_url() ?> ">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo-w.png" alt="logo">
                </a>
            </div>
            <div class="main-menu">

                <a href="<?= site_url() ?> ">
                    <div class="main-icon">
                        <div class="main-title">
                            الرئيسية
                        </div>
                        <div class="main-line"></div>
                    </div>
                </a>

                <a href="<?= site_url() ?>/من-نحن/">
                    <div class="who-icon">
                        <div class="who-title">
                            من نحن
                        </div>
                        <div class="who-line"></div>
                    </div>
                </a>

                <a href="<?= site_url() ?>/#backeg ">
                    <div class="packing-icon">
                        <div class="packing-title">
                            الباقات
                        </div>
                        <div class="packing-line"></div>
                    </div>
                </a>
            </div>

            <div class="visit-site">
                <a
                        href="<?= site_url() ?>/my-account">
                    <div class="visit-site-button">
                        الدخول إلى متجرك
                    </div>
                </a>
            </div>
        </section>
    </div>
    <style>
        .rouad-logo img{
            width: 100px !important;

        }
        .main-header img{
            width :35px;
        }
        @media only screen and (max-width: 776px) {
            .menu-open-icon {
                display: block;
            }

            .main-menu {
                display: none !important;
            }
        }

        @media only screen and (min-width: 776px) {
            .menu-icon {
                display: none !important;
            }

            .menu-open-icon {
                display: none;
            }

            .menu-mobile {
                display: none;

            }
        }

        .menu-close {
            display: none;
        }
        .site-header{
            margin: auto;
            z-index: 1000000;
            top: 0;
            right: 0;
            background-color: #ffffff;
            width: 100%;
            padding: 11px;
            display: flex;
            align-items: center;
            justify-content: space-between;

        }
        .main-header {
            margin: auto;
            z-index: 1000000;
            top: 0;
            right: 0;
            width: 100%;
            padding: 11px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .rouad-logo img {
            max-width: 89px;

        }

        div.<?= $activ_class ?> {
            border-bottom: 4px solid #4bc8f5;

        }

        .main-menu {
            width: 60%;
            display: flex;
            align-items: center;
            justify-content: flex-start;
            color: #1152f2;
            font-size: 16px;
            font-weight: bold;
        }

        a {
            color: #1152f2;
        }

        .visit-site-button :hover {
            background-color: #1152f2 !important;;
        }

        .main-menu div {
            color: #1152f2;
            margin-right: 10px;
            margin-left: 10px;
        }

        .visit-site-button {
            font-size: 16px;
            width: 160px;
            height: 40px;
            text-align: center;
            display: flex;
            align-content: center;
            justify-content: center;
            align-items: center;
            font-weight: 500;
            color: #FFFFFF;
            background-color: #4bc8f5;
            border-radius: 10px;
        }



        .main-icon {
            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .main-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        .who-icon {

            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .who-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        .packing-icon {
            display: flex;
            flex-direction: column;
            align-content: center;
            justify-content: center;
            align-items: center;
        }

        .packing-line {
            border-bottom: 4px solid transparent;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;

        }

        woocommerce-LostPassword woocommerce-LostPassword1 lost_password {
            font-size: 14px;
        }


        .menu-icon {
            cursor: pointer;
        }

        .rouad-logo-w {
            display: none;
        }

        .menu-close-w {
            display: none;

        }

        .menu-open-icon-w {
            display: none;

        }

        .menu-close-icon-w {
            display: none;

        }

        .menu-close {
            display: none;

        }

    </style>
    <script>


        let type = window.location.href;
        type1 = type.split("#");
        if (type1[1] == "package") {
            console.log("dfdfdf")

        }
        jQuery(".mobile-open-menu").hide();

        jQuery(document).scroll(function () {
            if (jQuery(this).scrollTop() > 1) {
                jQuery('.site-header').css({"position": "fixed"});
                jQuery('.main-menu div').css({"color": "#ffffff"})
                jQuery('.site-header').css({"background-color": "#1152F2"})
                jQuery('.rouad-logo-b').hide()
                jQuery('.rouad-logo-w').show()
                jQuery('.menu-open-icon-w').show()
                jQuery('.menu-close-icon-w').show()
                jQuery('.menu-open-icon').hide()
                jQuery('.menu-close-icon').hide();
                jQuery(".visit-site-button").hover(function(){
                    jQuery(this).css("background-color", "#ffffff");
                    jQuery(this).css("color", "#1152F2");
                }, function(){
                    jQuery(this).css("background-color", "#4bc8f5");
                    jQuery(this).css("color", "#ffffff");
                });
            } else {

                jQuery(".visit-site-button").hover(function(){

                    jQuery(this).css("background-color", "#1152F2");
                    jQuery(this).css("color", "#ffffff");
                }, function(){

                    jQuery(this).css("background-color", "#4bc8f5");
                    jQuery(this).css("color", "#ffffff");
                });
                jQuery('.site-header').css({"position": "inherit"});
                jQuery('.main-menu div').css({"color": "#1152F2"})
                jQuery('.site-header').css({"background-color": "#ffffff"})
                jQuery('.rouad-logo-w').hide()
                jQuery('.rouad-logo-b').show()

                jQuery('.menu-open-icon-w').hide()
                jQuery('.menu-close-icon-w').hide()
                jQuery('.menu-open-icon').show()
                jQuery('.menu-close-icon').show()
            }
        });


        jQuery(".menu-open").on("click", function (event) {
            jQuery(".menu-open").hide()
            jQuery(".menu-close").show()
            jQuery(".mobile-open-menu").show();
        });

        jQuery(".menu-close").on("click", function (event) {
            jQuery(".menu-close").hide()
            jQuery(".menu-open").show()
            jQuery(".mobile-open-menu").hide();
        });
    </script>
    <div id="content" class="site-content">
        <div class="ast-container">
