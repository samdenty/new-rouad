<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php /* translators: %s: Customer billing full name */ ?>
    <div class="new-order-email" style="    direction: rtl;">
        <div class="email-header" style="            display: flex!important;
">
            <img class="image-header-logo" src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo-w.png">
        </div>
        <div class="man-photo-section">
            <img class="man-photo" src="<?= site_url() ?>/wp-content/uploads/2023/01/email/man.png">

        </div>

        <div class="receive-name">
            <?php printf(esc_html__('هلا والله  %s ', 'woocommerce'), $order->get_formatted_billing_full_name()); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>

        </div>
        <div class="email-title">
            شكرا لك على طلبك من المنصة الأقوى والأجدر في التجارة الإلكترونية.
            <br>
            هنا تفاصيل الفاتورة الخاصة بك:
        </div>
        <div class="email-order-number">
            رقم الطلب :

            <?= $order->get_order_number(); ?>

        </div>
        <div class="email-date-number">
            تاريخ الطلب :
            <?=
            $order->order_date;
            ?>

        </div>

        <div class="order-details-email">
            <?php
            /*
             * @hooked WC_Emails::order_details() Shows the order details table.
             * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
             * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
             * @since 2.5.0
             */
            do_action('woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email);

            /*
             * @hooked WC_Emails::order_meta() Shows order meta data.
             */
            do_action('woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email);

            /*
             * @hooked WC_Emails::customer_details() Shows customer details
             * @hooked WC_Emails::email_address() Shows email address
             */
            do_action('woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email);

            /**
             * Show user-defined additional content - this is set in each email's settings.
             */
            //    if ($additional_content) {
            //        echo wp_kses_post(wpautop(wptexturize($additional_content)));
            //    }

            /*
             * @hooked WC_Emails::email_footer() Output the email footer
             */
            //    do_action('woocommerce_email_footer', $email);
            ?>
            <style>
                .receive-name {
                    margin-top: 20px;
                    font-size: 20px;
                    font-weight: 700;
                    color: #1152f2;
                }

                .thank-you-title {
                    font-size: 20px;
                    background-color: #4bc8f5;
                    text-align: center;
                    padding: 25px;
                    color: #ffffff;
                    font-weight: 700;
                    margin-top: 40px;
                    margin-bottom: 40px;

                }

                .email-title {
                    line-height: 1.5!important;
                    font-size: 20px !important;
                    margin-top: 10px !important;
                    margin-bottom: 10px !important;
                    color:#000000 !important;

                }

                .man-photo {
                    width: 80%;

                }


                .email-date-number, .email-order-number {
                    display: flex;
                    color: #000000;
                    font-size: 18px;
                    font-weight: bolder;
                    margin-top:20px;
                    margin-buttom:20px;

                }

                .order-number, .order-date {
                    color: #000000;
                    margin-right: 20px;
                }

                .man-photo-section {
                    margin-top: 50px;

                    display: flex;
                    justify-content: center;
                    align-items: center;
                }

                .image-header-logo {
                    width: 100px;
                    margin: 20px;
                }

                .new-order-email {
                    width: 100%;
                    max-width: 600px;
                }

                .email-header {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    background-color: #1152f2;
                }
            </style>

            <?php
            ?>

        </div>
        <div class="thank-you-title">
            شكراً لك على طلبك
        </div>
    </div>


<?php

