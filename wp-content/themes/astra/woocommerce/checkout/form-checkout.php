<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}
?>
<form name="checkout" method="post" class="checkout woocommerce-checkout"
      action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <?php if ($checkout->get_checkout_fields()) : ?>
        <?php


        ?>
        <?php do_action('woocommerce_checkout_before_customer_details'); ?>

        <div class="col2-set" id="customer_details">
            <div class="col-1">
                <?php do_action('woocommerce_checkout_billing'); ?>
            </div>

            <div class="col-2">
                <?php do_action('woocommerce_checkout_shipping'); ?>
            </div>
        </div>

        <?php do_action('woocommerce_checkout_after_customer_details'); ?>

    <?php endif; ?>

    <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

    <h3 id="order_review_heading"><?php esc_html_e('', 'woocommerce'); ?></h3>

    <?php do_action('woocommerce_checkout_before_order_review'); ?>

    <div id="order_review" class="woocommerce-checkout-review-order">
        <?php do_action('woocommerce_checkout_order_review'); ?>
    </div>

    <?php do_action('woocommerce_checkout_after_order_review'); ?>

</form>

<?php do_action('woocommerce_after_checkout_form', $checkout);


?>
<style>
    .show-login-form {
        cursor: pointer;
    }

    .woocommerce form.checkout_coupon, .woocommerce form.login, .woocommerce form.register {
    }

    .woocommerce form .form-row label {
        font-weight: 500;
        font-size: 14px;
    }

    .form-login {
        display: flex;
        justify-content: space-evenly;

    }

    .remember-lost {
        display: flex;
        justify-content: space-between;
    }

    .form-login-sec-1 {
        width: 50% !important;

    }

    .photo-login {

        width: 50%;

    }

    @media screen and (min-width: 921px) {

        button.woocommerce-button.button.woocommerce-form-login__submit.wp-element-button {
            width: 80%;
            border-radius: 10px;
            /* margin: auto !important; */
            background-color: #1152F2;
        }
    }

    .paragraph2 {
        font-size: 25px;
        font-weight: bolder;
    }

    .paragraph {
        font-family: "Urw Arabic", Sans-serif;
        color: #161B27 !important;
    }

    .lost_password {
        font-size: 14px;

    }

    .paragraph1 {
        font-size: 27px !important;

    }


    .photo-login {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .login-lable {
        text-align: center;

    }

    .entry-title {
        text-align: center;
        font-weight: 700;
        background-color: #ffffff;
        border-radius: 6px;
        height: 70px;
        color: #2F3088 !important;
        display: none !important;
        justify-content: center;
        align-items: center;
    }

    .woocommerce .col2-set .col-1, .woocommerce-page .col2-set .col-1 {
        float: right;
        width: 100%;
    }

    .register-form {
        display: none;
    }

    .login-form {
    }

    .woocommerce .col2-set .col-2, .woocommerce-page .col2-set .col-2 {
        width: 100% !important;
    }

    #reg_username, #reg_email, #reg_password {
        border: none;
        background-color: #EEF7FF;
        border-radius: 5px;
        height: 50px;
        width: 80%;

    }

    .register-submit {
        width: 70%;

        border-radius: 10px !important;
        margin: auto !important;
        margin-right: 10px !important;
        background-color: #38D3D6 !important;

    }

    .register-lable {
        font-size: 30px;
        font-weight: bold;
        text-align: center;

    }

    .woocommerce-privacy-policy-text {
        display: none !important;
    }

    .woocommerce-LostPassword {
        text-align: center;
    }

    .show-login-form {
        cursor: pointer;
    }

    .show-register-form {
        cursor: pointer;

    }

    @media screen and (max-width: 600px) {
        .rouad-theme input {
            width: 100% !important;
        }

        .form-login {
            flex-direction: column-reverse;
        }

        .register-form {
            width: 100% !important;
        }

        .login-form {
            width: 100% !important;
        }

        .photo-login {
            width: 100% !important;

        }
    }

    .woocommerce-page.woocommerce-checkout form.login .form-row, .woocommerce.woocommerce-checkout form.login .form-row {
        width: 40%;
        float: none;
    }

    @media screen and (max-width: 921px) {

        .woocommerce-page.woocommerce-checkout form.login .form-row, .woocommerce.woocommerce-checkout form.login .form-row {
            width: 100% !important;
            float: none;
        }

        input#username, input#password {
            width: 100% !important;
        }

        button.woocommerce-button.button.woocommerce-form-login__submit.wp-element-button {
            width: 100% !important;
            border-radius: 10px;
            /* margin: auto !important; */
            background-color: #1152f2;
        }

    }
    *{
        font-style: initial !important;

    }
    .woocommerce-page.woocommerce-checkout #payment #place_order, .woocommerce.woocommerce-checkout #payment #place_order{
        background-color: #1152f2;
        border-radius: 10px;

    }

    .rouad-theme input {
        border: none !important;
        background-color: #EEF7FF !important;
        border-radius: 5px !important;
        height: 50px !important;
    }

    @media screen and (min-width: 600px) {

        .rouad-theme input {
            width: 70% !important;
        }
    }

    .input-text {
        border: none !important;
        background-color: #EEF7FF !important;
        border-radius: 5px !important;
        height: 50px !important;
    }

    form.woocommerce-form.woocommerce-form-login.login {
        display: flex;
        flex-direction: column;
        line-height: 3.5;
    }


</style>
<script>
    jQuery(".show-login-form").on("click", function () {

        jQuery("#customer_login").hide()
        jQuery(".register-form").show()


    });
    jQuery(".show-register-form").on("click", function () {
        jQuery(".register-form").hide()
        jQuery("#customer_login").show()

    });
</script>
<style>
    div {
        font-family: "Urw Arabic", Sans-serif !important;
    }


    .wp-element-button {
        border-radius: 10px;
    }

    a.button.wc-forward.wp-element-button {
        border-radius: 10px;
        background-color: #1152F2;
    }

    .woocommerce-message, .woocommerce-info {
        border-top-color: #1152F2;
        background-color: #F1F6FF;
        color: #000000;
    }

    .woocommerce-message::before, .woocommerce-info::before {
        color: #1152F2;

    }

    .woocommerce-error {
        background-color: #F1F6FF;
        color: #000000;

    }
</style>