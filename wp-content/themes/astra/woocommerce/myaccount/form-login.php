<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
do_action('woocommerce_before_customer_login_form'); ?>

<div class="form-login">
    <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>
    <div class="u-columns col2-set form-login-sec-1 login-form" id="customer_login">

        <div class="u-column1 col-1 " style="margin-top: 47px;">

            <?php endif; ?>

            <div>
                <h2 class="register-lable"><?php esc_html_e('هلا والله في روّاد', 'woocommerce'); ?></h2>

                <form class="woocommerce-form woocommerce-form-login login" method="post">

                    <?php do_action('woocommerce_login_form_start'); ?>

                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username"><?php esc_html_e('اسم المستخدم أو البريد الإلكتروني', 'woocommerce'); ?>
                            &nbsp;<span
                                    class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
                               id="username" autocomplete="username"
                               value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password"><?php esc_html_e('كلمة المرور', 'woocommerce'); ?>&nbsp;<span
                                    class="required">*</span></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
                               name="password"
                               id="password" autocomplete="current-password"/>
                    </p>

                    <?php do_action('woocommerce_login_form'); ?>

                    <p class="form-row">
                    <div class="remember-lost">
                        <div>
                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">

                                <input class="woocommerce-form__input woocommerce-form__input-checkbox"
                                       name="rememberme"
                                       type="checkbox" id="rememberme" value="forever"/>
                                <span><?php esc_html_e(' تذكرني', 'woocommerce'); ?></span>

                            </label>
                        </div>
                        <div class="lostpassword">


                            <p class="woocommerce-LostPassword lost_password">
                                <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('هل نسيت كلمة المرور ؟', 'woocommerce'); ?></a>
                            </p>

                        </div>
                    </div>
                    <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                    <button type="submit"
                            class="woocommerce-button button woocommerce-form-login__submit<?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>"
                            name="login"
                            value="<?php esc_attr_e('تسجيل الدخول', 'woocommerce'); ?>"><?php esc_html_e('تسجيل الدخول', 'woocommerce'); ?></button>
                    </p>
                    <?php do_action('woocommerce_login_form_end'); ?>
                </form>
                <div class="lostpassword">


                    <p class="woocommerce-LostPassword woocommerce-LostPassword1 lost_password"style="font-size:14px;   ">
                        &nbsp;&nbsp;
                        ليس لديك حساب في روّاد؟<a
                                class="show-login-form" style="text-decoration: underline; margin-right: 5px;"><?php esc_html_e('إنشاء حساب الان ', 'woocommerce'); ?></a>
                    </p>
                </div>
            </div>


        </div>

    </div>
    <div class="u-columns col2-set form-login-sec-1 register-form" style="padding-top:50px;">

        <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

        <h2 class="register-lable"><?php esc_html_e('هلا والله في روّاد', 'woocommerce'); ?></h2>

        <div class="u-column2 col-2">
            <form method="post"
                  class="woocommerce-form woocommerce-form-register register" <?php do_action('woocommerce_register_form_tag'); ?> >

                <?php do_action('woocommerce_register_form_start'); ?>

                <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_username"><?php esc_html_e('اسم المستخدم', 'woocommerce'); ?>&nbsp;<span
                                    class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
                               id="reg_username" autocomplete="username"
                               value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                    </p>

                <?php endif; ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_email"><?php esc_html_e('البريد الإلكتروني', 'woocommerce'); ?>&nbsp;<span
                                class="required">*</span></label>
                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email"
                           id="reg_email" autocomplete="email"
                           value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                </p>

                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_password"><?php esc_html_e('كلمة المرور', 'woocommerce'); ?>&nbsp;<span
                                    class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text"
                               name="password" id="reg_password" autocomplete="new-password"/>
                    </p>

                <?php else : ?>

                    <p><?php esc_html_e('A link to set a new password will be sent to your email address.', 'woocommerce'); ?></p>

                <?php endif; ?>

                <?php do_action('woocommerce_register_form'); ?>

                <p class="woocommerce-form-row form-row">
                    <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                    <button type="submit" class="woocommerce-Button woocommerce-button button
                <?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ?
                        ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>
                    woocommerce-form-register__submit" name="register" value="<?php esc_attr_e(
                        'إنشاء حساب', 'woocommerce'); ?>"><?php esc_html_e('إنشاء حساب',
                            'woocommerce'); ?></button>
                </p>


                <?php do_action('woocommerce_register_form_end'); ?>

            </form>

        </div>
        <p class="woocommerce-LostPassword woocommerce-LostPassword1 lost_password">
            لديك حساب في روّاد؟ <a
                    class="show-register-form"><?php esc_html_e('تسجيل الدخول ', 'woocommerce'); ?></a>
        </p>
    </div>
<?php endif; ?>


    <div class="photo-login">
        <div class="title-left">
            <h3 class="paragraph paragraph1">
                ابدأ مع منصّة روّاد
            </h3>
            <p class="paragraph2 paragraph">
                المنصّة الأقوى والأجدر في مجال

                التجارة الإلكترونية
            </p>
        </div>
        <img src="<?= site_url() ?>/wp-content/themes/astra/woocommerce/assets/login.gif"></div>
</div>


<style>
    .woocommerce-LostPassword{
        font-size:14px;


    }
    button.woocommerce-Button.woocommerce-button.button.wp-element-button.woocommerce-form-register__submit{
        margin-top:20px
    }
    @media only screen and (min-width: 776px) {
        .register-lable{
            width: 80%;
        }
    }
    @media only screen and (max-width: 776px) {
        button.woocommerce-Button.woocommerce-button.button.wp-element-button.woocommerce-form-register__submit{
            width :100% !important;

        }
        #reg_username, #reg_email, #reg_password{
            width :100% !important;
        }
        h2.register-lable {
            margin-bottom: 0px;
            width:100%
        }

        button.woocommerce-button.button.woocommerce-form-login__submit.wp-element-button {
            width: 100% !important;
        }

        .paragraph2 {
            font-size: 18px !important;
        }

        .main-menu {
            display: none;
        }

        .visit-site-button {
            height: 40px !important;
        }

        input#username, input#password {
            width: 100% !important;
        }

        .remember-lost {
            width: 100% !important
        }
    }

    @media only screen and (max-width: 776px){
        .title-left
        {
            text-align: center !important;

        }

    }
    .woocommerce form .form-row label{
        line-height: 2;
        font-weight: 70;
        font-size: 16px !important;
        margin-bottom: 10px;

    }
    .woocommerce form.checkout_coupon, .woocommerce form.login, .woocommerce form.register{
        margin:0px;
        padding-bottom:0 ;
    }
.site-main{
    margin-top: 50px;

}
    button.woocommerce-Button.woocommerce-button.button.wp-element-button.woocommerce-form-register__submit {
        width: 80%;
        border-radius: 10px;
        background-color: #1152F2;
    }

    .show-login-form {
        cursor: pointer;
    }

    .woocommerce form.checkout_coupon, .woocommerce form.login, .woocommerce form.register {
        border: none;
    }

    .form-login {
        display: flex;
        justify-content: space-evenly;

    }

    .remember-lost {
        display: flex;
        justify-content: space-between;
        width: 80%;
        font-size: 14px;
    }

    .form-login-sec-1 {
        width: 50% !important;

    }

    .photo-login {

        width: 50%;

    }

    #content {
        min-height: 700px;
    }

    input#username, input#password {
        background-color: #EEF7FF;
        border-radius: 5px;
        height: 50px;
        width: 80%;

    }

    button.woocommerce-button.button.woocommerce-form-login__submit.wp-element-button {
        width: 80%;
        border-radius: 10px;
        /* margin: auto !important; */
        background-color: #1152F2;
    }
    .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce .woocommerce-message a.button:hover, .woocommerce #respond input#submit:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce input.button:hover, .woocommerce button.button.alt.disabled:hover, .wc-block-grid__products .wc-block-grid__product .wp-block-button__link:hover
    {
        background-color: #4bc8f5;

    }
    .paragraph2 {
        font-size: 25px;
        font-weight: 500;
        margin-bottom: 24px !important;

    }

    .paragraph {
        font-family: "Urw Arabic", Sans-serif;
        color: #161B27 !important;
    }

    .paragraph1 {
        font-size: 30px !important;
        font-weight: 700;
    }

    .photo-login {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .login-lable {
        text-align: center;

    }

    .entry-title {
        text-align: center;
        font-weight: 700;
        background-color: #ffffff;
        border-radius: 6px;
        height: 70px;
        color: #2F3088 !important;
        display: none !important;
        justify-content: center;
        align-items: center;
    }

    .woocommerce .col2-set .col-1, .woocommerce-page .col2-set .col-1 {
        float: right;
        width: 100%;
    }

    .register-form {
        display: none;
    }

    .login-form {
    }

    .woocommerce .col2-set .col-2, .woocommerce-page .col2-set .col-2 {
        width: 100% !important;
    }

    #reg_username, #reg_email, #reg_password {
        background-color: #EEF7FF;
        border-radius: 5px;
        height: 50px;
        width: 80%;

    }

    .register-submit {
        width: 70%;

        border-radius: 10px !important;
        margin: auto !important;
        margin-right: 10px !important;
        background-color: #38D3D6 !important;

    }

    .register-lable {
        font-size: 30px;
        font-weight: bold;
        text-align: center;

    }

    .woocommerce-privacy-policy-text {
        display: none !important;
    }

    .woocommerce-LostPassword {
        text-align: center;
    }

    .show-login-form {
        cursor: pointer;
    }

    .show-register-form {
        cursor: pointer;

    }

    .woocommerce-LostPassword1 {
        width: 80%;
    }

    @media screen and (max-width: 600px) {
        .form-login {
            flex-direction: column-reverse;
        }

        .woocommerce-LostPassword1 {
            width: 100%;
        }

        .register-form {
            width: 100% !important;
        }

        .login-form {
            width: 100% !important;
        }

        .photo-login {
            width: 100% !important;

        }
    }


</style>
<script>
    jQuery(".show-login-form").on("click", function () {

        jQuery("#customer_login").hide()
        jQuery(".register-form").show()


    });
    jQuery(".show-register-form").on("click", function () {
        jQuery(".register-form").hide()
        jQuery("#customer_login").show()

    });
</script>



