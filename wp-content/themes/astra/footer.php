<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
</div> <!-- ast-container -->
</div><!-- #content -->
<?php
//astra_content_after();

//astra_footer_before();

//astra_footer();

//astra_footer_after();
?>
</div><!-- #page -->
<?php
astra_body_bottom();
//wp_footer();
?>

<section class="footer-section">
    <div class="first-footer-section">
        <div class="flex-section">
            <div class="footer-right">
                <div class="footer-logo">
                    <img class="footer-logo-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo.png">
                </div>
                <div class="footer-title">
                    تأسست منصة رواد في دبي عام 2021 وتركز على تقديم حلول تقنية لرواد الاعمال.
                </div>
                <div class="footer-social-icon">
                    <a href="https://www.facebook.com/rowadplatform">
                        <div class="social-box">
                            <i class="fab fa-facebook social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.instagram.com/rouad.tech/">
                        <div class="social-box">
                            <i class="fab fa-instagram social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.snapchat.com/add/rouad.tech?share_id=OBrj_1FlzP0&locale=en-US">
                        <div class="social-box">
                            <i class="fab fa-snapchat social-box1"></i>
                        </div>
                    </a>
                    <a href="https://www.linkedin.com/company/darb-productions/">
                        <div class="social-box">
                            <i class="fab fa-linkedin social-box1"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __("روّاد") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="<?= site_url() ?>/%d9%82%d8%b5%d8%aa%d9%86%d8%a7/">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("قصتنا") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/why-rouad/">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("لماذا روّاد؟") ?></div>
                        </div>
                    </a>

                </div>
            </div>
        </div>
        <div class="flex-section">
            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __("روابط مفيدة") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="https://goo.gl/maps/Kz4JQBaMb6Rw5Mf8A">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("العنوان على خرائط جوجل") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/?page_id=3">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __(" سياسة الخصوصية؟") ?></div>
                        </div>
                    </a>

                </div>
            </div>
            <div class="footer-mid-right">
                <div class="footer-title-rouad">
                    <?= __(" الدعم والمساعدة") ?>
                </div>

                <div class="footer-menu-mid-right">
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("المبيعات") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("الدعم الفني") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("الادارة المالية") ?></div>
                        </div>
                    </a>
                    <a href="https://wa.me/+971502637714?text=connect%20with%20our%20team">
                        <div class="menu-item">
                            <i aria-hidden="true" class="fas fa-chevron-left"></i>
                            <div class="menu-element"><?= __("واتس اب") ?></div>
                        </div>
                    </a>
                    <a href="<?= site_url() ?>/%D8%AA%D9%88%D8%A7%D8%B5%D9%84-%D9%85%D8%B9%D9%86%D8%A7/"">
                    <div class="menu-item">
                        <i aria-hidden="true" class="fas fa-chevron-left"></i>
                        <div class="menu-element"><?= __("تواصل معنا") ?></div>
                    </div>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <div class="second-footer-section-color">
        <div class="second-footer-section">
            <div class="rouad-adress">
                <?= __("المقر الرئيسي: الإمارات العربية المتحدة -دبي - الخليج التجاري - برج تماني ارتس - مكتب رقم 543") ?>
            </div>
            <div class="rouad-footer-logo">
                <?= __("صُنع بواسطة منصة ") ?>
                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/logo/logo-w.png">
            </div>
        </div>
    </div>
</section>
<style>
    @media only screen and (max-width: 929px) {
        .first-footer-section {
            flex-direction:column;
            padding: 0px 30px !important;

        }
        .flex-section{
            width:100% !important;
        }
        .second-footer-section{
            flex-direction: column;
            align-items: center;

        }
        .rouad-adress{
            text-align: center;
            margin-bottom: 15px;
        }
    }

    .rouad-footer-logo img {
        width: 60px;

    }

    .footer-right {
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: flex-start;
        width: 50%;

    }


    .second-footer-section {
        padding-top: 20px;
        color: #ffffff;
        font-size: 14px;
    }

    .first-footer-section, .second-footer-section {
        display: flex;
        width: 100%;
        max-width: 1190px;
        margin: auto;
        padding-bottom: 20px;
        justify-content: space-around;
    }

    .second-footer-section-color {
        background-color: #1152f2;
    }

    .footer-mid-right {
        width: 50%;
    }

    .flex-section {
        display: flex;
        width: 50%;
    }

    .menu-element {
        margin-top: -7px;
        color: #000000;
    }

    .menu-item {
        display: flex;
        color: #1152f2;
        margin: 17px 5px 5px 5px;
    }

    .menu-item i {
        text-align: center;
        justify-content: center;
        width: 35px;
        display: flex;
        align-items: center;
    }

    .footer-title-rouad {
        font-size: 18px;
        font-weight: 700;
        margin: 10px 20px 25px 20px;
    }

    .footer-section {
        font-size:14px;
        width: 100%;
        padding: 30px 0px 0px 0px;
    }

    .footer-social-icon {
        display: flex;
        padding: 5px;
        justify-content: flex-start;

    }

    .footer-social-icon a {
        margin: 5px;
    }

    .social-box {
        width: max-content;
    }

    .social-box1 {
        background-color: #ffffff;
        color: #1152f2;
        cursor: pointer;
        padding: 10px 10px 9px 10px;
        border-radius: 5px;
        border: 1px solid #1152f2;
    }

    .social-box :hover {

        background-color: #1152f2 !important;
        color: #ffffff !important;

    }

    .footer-section {
        background-color: #F1F6FF;

    }

    .footer-logo-img {
        width: 131px;
    }

    .footer-title {
        font-weight: 500;
        font-size: 14px;
        color: #000000;
        max-width: 278px;
    }
</style>
</body>
</html>
