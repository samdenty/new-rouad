<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
$url = explode("/", $_SERVER['REQUEST_URI']);
if ($url[1] == "") {


    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <section class="first-section">
        <div class="right-side">
            <div class="s1-rs-ft">
                جميع الأدوات التي تحتاجها في تجارتك الإلكترونية
            </div>
            <div class="s1-rs-st">
                مع منصّة روّاد امتلك متجرك الإلكتروني اليوم وقسّط قيمته على دفعات شهرية لمدة عام واحد فقط
            </div>
            <div class="s1-rs-tt">
                <a href="<?= site_url() ?>/my-account" class="new-store-url">
                    <div class="new-store-button">
                        أنشئ متجرك المجاني الآن
                    </div>
                </a>
            </div>
        </div>

        <div class="left-side">
            <img src="<?= site_url() ?>/wp-content/uploads/2023/01/Alaa photo.gif">
        </div>

    </section>
    <section class="second-section">
        <div class="s2-tools"
        >
            <img src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-3-1-1024x35.png">
        </div>
        <div class="s2-ft"
        >
            قسّط متجرك الإلكتروني لمدة عام واحد فقط

        </div>
        <div class="s2-st">
            استفد من تخفيضات باقات الاستضافة في السنوات التالية بدون أي أقساط إضافية
        </div>
    </section>
    <section class="th-section">
        <div class="s3-features">
            <div class="s3-f1 f1 ">
                <div class="s3-f1-s1">
                    <div class="s3-f1-s1-t1">
                        ملكية 100%
                    </div>
                    <div class="s3-f1-s1-t2">
                        احصل على متجرك الإلكتروني اليوم وقسط قيمته على دفعات شهرية لمدة عام , شامل الاستضافة
                        والنطاق والبريد الإلكتروني مجانا للعام الأول
                    </div>

                </div>
                <div class="s3-f1-s2">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/ملكية-١٠٠٪-1-1024x572.png">

                </div>

            </div>
            <div class="s3-f1 f2">
                <div class="s3-f1-s1">
                    <div class="s3-f1-s1-t1">
                        الضمان الذهبي
                    </div>
                    <div class="s3-f1-s1-t2">نقدم لك ضمان لاسترداد اموالك
                        خلال ثلاثين يوم من اشتراك ونضمن لك استرداد
                        فوري من دون آي سؤال. في حال رغبت بايقاف
                        التعاقد خلال اول 30 يوم يتم رد كامل المبلغ لك
                    </div>

                </div>
                <div class="s3-f1-s2">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/الضمان-الذهبي_-768x496.png">
                </div>

            </div>
            <div class="s3-f1 f3">
                <div class="s3-f1-s1">
                    <div class="s3-f1-s1-t1">
                        الربط مع بوابات الدفع و الشحن
                    </div>
                    <div class="s3-f1-s1-t2">


                        نوفر حلول لوجستية ومالية لمتجرك من خلال ربطه
                        <br>
                        مع
                        أفضل شركات الشحن وبوابات الدفع الإلكترونية
                    </div>

                </div>
                <div class="s3-f1-s2">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/الربط-مع-بوابات-الدفع-و-الشحن-1-768x531.png">

                </div>
            </div>
            <div class="s3-f1 f4">
                <div class="s3-f1-s1">
                    <div class="s3-f1-s1-t1">
                        دعم فني مجاني
                    </div>
                    <div class="s3-f1-s1-t2">
                        فريق عربي محترف ومتعاون يعمل معك
                        عن قرب لضمان حصولك على أفضل تجربة
                        وتقديم الدعم الفني والتقني
                        <br>
                        اللازم لك


                    </div>

                </div>
                <div class="s3-f1-s2">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/دعم-فني-مجاني-1-1536x811.png">
                </div>
            </div>
        </div>
        <div class="s3-f-button">
            <div class="s3-f1-button f1">
                <div class="f-image">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/ملكية-١٠٠٪-300x300.png">

                </div>
                <div class="f-t"
                >
                    ملكية 100%
                </div>
            </div>
            <div class="s3-f1-button f2">
                <div class="f-image">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/الضمان-الذهبي-300x300.png">
                </div>
                <div class="f-t"
                >
                    الضمان
                </div>
            </div>
            <div class="s3-f1-button f3">
                <div class="f-image">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/الربط-مع-بوابات-الدفع-و-الشحن.png">
                </div>
                <div class="f-t"
                >
                    الدفع والشحن
                </div>
            </div>
            <div class="s3-f1-button f4">
                <div class="f-image">
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/دعم-فني-مجاني-300x300.png">
                </div>
                <div class="f-t">
                    الدعم
                </div>
            </div>


        </div>

    </section>
    <section class="fo-section">
        <div class="s4-title">
            استمر بالتقدم للأمام ونحن نتولى المهام
        </div>
        <div class="s4-s2">
            <div class="s4-s2-cart">
                <div class="cart-img"
                ><img width="87px" src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-3-3-150x150-1.png"></div>
                <div class="cart-title">
                    <div class="cart-t1">بدون عمولة على المبيعات
                    </div>
                    <div class="cart-t2">
                        يتم تحويل كافة المبالغ لحسابك مباشرة من بوابة
                        <br>
                        الدفع ومن دون اي عمولات
                    </div>
                </div>
            </div>
            <div class="s4-s2-cart">
                <div class="cart-img"
                ><img width="87px" src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-5-1-150x150-1.png"></div>
                <div class="cart-title">
                    <div class="cart-t1">
                        متعدد اللغات
                    </div>
                    <div class="cart-t2">
                        متعدد اللغات
                        متاجرنا تدعم عدد لا محدود من اللغات <br>
                    </div>
                </div>
            </div>
            <div class="s4-s2-cart">
                <div class="cart-img"
                ><img width="87px" src="<?= site_url() ?>/wp-content/uploads/2022/12/Layer-7-150x150-1.png"></div>
                <div class="cart-title">
                    <div class="cart-t1">
                        تحديثات دورية
                    </div>
                    <div class="cart-t2">

                        نواكب أحدث التقنيات العالمية ونجلبها بشكل <br>
                        دوري لمتجرك
                    </div>
                </div>
            </div>


        </div>
    </section>


    <section id="backeg" class="fi-section">
        <div class="s5-t1">باقات المتاجر من روّاد
        </div>
        <div class="s5-t2">
            احصل على أفضل عرض من الحزم
        </div>

        <div class="subscription-type">
            <div class="sub-name sub-month sub-month-name"> شهري</div>
            <div class="sub-name sub-quart sub-quart-name"> ربع سنوي</div>
            <div class="sub-name sub-yearly sub-yearly-name"> سنوي</div>
        </div>
        <div class="sub-carts  sub-month">
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/seller.png">
                </div>
                <div class="offer-name">
                    تاجر
                </div>
                <div class="offer-price">
                    299 درهم/ شهرياً لمدة
                    <br>
                    عام

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">
                    ملكية ١٠٠٪

                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    عدد منتجات غير محدود
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تخصيص المتجر
                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1161">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/growth.png">
                </div>
                <div class="offer-name">
                    نمو
                </div>
                <div class="offer-price">
                    350 درهم/ شهريا لمدة <br>
                    عام

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    ربط شركات دروب شوبينج
                    <br>
                    <br>
                </div>
                <div class="to-checkout">
                    <a href="<?= site_url() ?>/checkout/?add-to-cart=960">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/king.png">
                </div>
                <div class="offer-name">
                    ريادة
                </div>
                <div class="offer-price">
                    999 درهم/ شهرياً لمدة <br>
                    عام

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تطبيق ايفون واندرويد مع الربط <br>

                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=958">
                        اشترك الاّن
                    </a>
                </div>
            </div>

        </div>
        <div class="sub-carts  sub-quart">
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/seller.png">
                </div>
                <div class="offer-name">

                    تاجر
                </div>
                <div class="offer-price">
                    810 درهم/ 3 شهور
                    <br>
                    <br>

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    ملكية ١٠٠٪
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    عدد منتجات غير محدود
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تخصيص المتجر
                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1391">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/growth.png">
                </div>
                <div class="offer-name">
                    نمو
                </div>
                <div class="offer-price">
                    975 درهم/ 3 شهور

                    <br>
                    <br>

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    ربط شركات دروب شوبينج
                    <br>
                    <br>
                </div>
                <div class="to-checkout">
                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1393">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/king.png">
                </div>
                <div class="offer-name">
                    ريادة
                </div>
                <div class="offer-price">
                    2850 درهم/ 3 شهور

                    <br>

                    <br>
                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تطبيق ايفون واندرويد مع الربط <br>

                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1396">
                        اشترك الاّن
                    </a>
                </div>
            </div>

        </div>
        <div class="sub-carts  sub-yearly">
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/seller.png">
                </div>
                <div class="offer-name">
                    تاجر
                </div>
                <div class="offer-price">
                    3000 درهم/ سنوياً
                    <br>
                    <br>

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    ملكية ١٠٠٪
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    عدد منتجات غير محدود
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تخصيص المتجر
                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1391">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/growth.png">
                </div>
                <div class="offer-name">
                    نمو
                </div>
                <div class="offer-price">
                    3599 درهم/ سنوياً
                    <br>
                    <br>

                </div>
                <div class="description">
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    ربط شركات دروب شوبينج
                    <br>
                    <br>
                </div>
                <div class="to-checkout">
                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1393">
                        اشترك الاّن
                    </a>
                </div>
            </div>
            <div class="sub-cart">
                <div>
                    <img src="<?= site_url() ?>/wp-content/uploads/2022/12/shapef.png ">

                </div>
                <div class="offer-icon">
                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/king.png">
                </div>
                <div class="offer-name">
                    ريادة
                </div>
                <div class="offer-price">
                    10000 درهم/ سنوياً
                    <br>
                    <br>
                </div>
                <div class="description">

                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">
                    كامل مميزات باقة تاجر
                    <br>
                    <img class="arow-img" src="<?= site_url() ?>/wp-content/uploads/2023/01/arow.png">

                    تطبيق ايفون واندرويد مع الربط <br>
                    بالمتجر
                </div>
                <div class="to-checkout">

                    <a href="<?= site_url() ?>/checkout/?add-to-cart=1396">
                        اشترك الاّن
                    </a>
                </div>
            </div>

        </div>
    </section>
    <section class="go-section">
        <a href="<?= site_url() ?>/ميزات-متجر-منصة-روّاد/">
            <div class="go-feature">
                تعرف على الميزات
            </div>
        </a>
    </section>
    <section class="six-section">
        <div class="s6-title">
            متاجر تم تنفيذها بواسطة منصة روّاد

        </div>
        <div class="s6-title2">
            مشاريعنا الإبداعية الأخيرة
        </div>

        <div class="container33 mobile-view">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>
                    <li data-target="#myCarousel" data-slide-to="6"></li>
                    <li data-target="#myCarousel" data-slide-to="7"></li>
                    <li data-target="#myCarousel" data-slide-to="8"></li>
                    <li data-target="#myCarousel" data-slide-to="9"></li>
                    <li data-target="#myCarousel" data-slide-to="10"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <a href="https://greenoption.ae/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Green.png"
                                     alt="Los Angeles">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://almicoajm.com/">

                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Almico.png" alt="Chicago">

                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://thepetsjoy.com/">

                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Pet%20Joy.png"
                                     alt="Chicago">

                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://themonoshop.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/mono.png" alt="Chicago">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://noriastore.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/noria.png" alt="New york">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://laylak-fashion.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/laylak.png" alt="New york">
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="http://omani-guide.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/omani.png" alt="New york">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="http://rakhaa.ae/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Rakhaa%20.png"
                                     alt="Chicago">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://oudelsalateen.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/salateen.png"
                                     alt="New york">
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="https://thecomenplay.com/">
                            <div class="item">
                                <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/The Comen Play.png"
                                     alt="New york">
                            </div>
                        </a>
                    </div>
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="container desktop-view">
            <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel1" data-slide-to="1"></li>
                    <li data-target="#myCarousel1" data-slide-to="2"></li>

                </ol>

                <div class="carousel-inner">
                    <div class="item active">
                        <div class="desktop" style="display:flex">
                            <a href="https://greenoption.ae/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Green.png"
                                         alt="Los Angeles">
                                </div>
                            </a>
                            <a href="https://almicoajm.com/">

                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Almico.png"
                                         alt="Chicago">

                                </div>
                            </a>
                            <a href="https://thepetsjoy.com/">

                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Pet%20Joy.png"
                                         alt="Chicago">

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="desktop" style="display:flex">
                            <a href="https://themonoshop.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/mono.png"
                                         alt="Chicago">
                                </div>
                            </a>
                            <a href="https://noriastore.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/noria.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="https://laylak-fashion.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/laylak.png"
                                         alt="New york">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div style="display:flex">
                            <a href="https://almali20.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/malle.png"
                                         alt="Chicago">
                                </div>
                            </a

                            <a href="http://omani-guide.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/omani.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="http://omani-guide.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/words.png"
                                         alt="New york">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div style="display:flex">
                            <a href="http://rakhaa.ae/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Rakhaa%20.png"
                                         alt="Chicago">
                                </div>
                            </a>
                            <a href="https://oudelsalateen.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/salateen.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="https://thecomenplay.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/The Comen Play.png"
                                         alt="New york">
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel1" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>


        <div class="container1 tab-view">
            <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel2" data-slide-to="1"></li>
                    <li data-target="#myCarousel2" data-slide-to="2"></li>

                    <li data-target="#myCarousel2" data-slide-to="3"></li>
                    <li data-target="#myCarousel2" data-slide-to="4"></li>

                    <li data-target="#myCarousel2" data-slide-to="5"></li>
                    <li data-target="#myCarousel2" data-slide-to="6"></li>
                    <li data-target="#myCarousel2" data-slide-to="7"></li>


                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="desktop" style="display:flex">
                            <a href="https://greenoption.ae/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Green.png"
                                         alt="Los Angeles">
                                </div>
                            </a>
                            <a href="https://almicoajm.com/">

                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Almico.png"
                                         alt="Chicago">

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="desktop" style="display:flex">

                            <a href="https://thepetsjoy.com/">

                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Pet%20Joy.png"
                                         alt="Chicago">

                                </div>
                            </a>
                            <a href="https://themonoshop.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/mono.png"
                                         alt="Chicago">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="desktop" style="display:flex">

                            <a href="https://noriastore.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/noria.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="https://laylak-fashion.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/laylak.png"
                                         alt="New york">
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="item">
                        <div style="display:flex">

                            <a href="http://omani-guide.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/omani.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="http://rakhaa.ae/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/Rakhaa%20.png"
                                         alt="Chicago">
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="item">
                        <div style="display:flex">
                            <a href="https://oudelsalateen.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/salateen.png"
                                         alt="New york">
                                </div>
                            </a>
                            <a href="https://thecomenplay.com/">
                                <div class="item">
                                    <img src="<?= site_url() ?>/wp-content/uploads/2023/01/sites/The Comen Play.png"
                                         alt="New york">
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </section>
    <section class="seven-section">
        <div class="s7-title">
            الأسئلة الشائعة
        </div>
        <div class="questions" id="q-1">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                مالفرق بين نظام الأقساط في منصّة روّاد ونظام الاشتراك المتّبع في المنصّات الأخرى؟
            </div>
            <div class="answer">
                في نظام الاشتراك يتوجب على المشترك دفع مبلغ شهري بشكل دائم وطوال سنوات وجوده على المنصة، أما في نظام
                التقسيط
                الذي نقدمه لعملائنا فالدفع يتم فقط لمدة سنة واحدة وبعدها يتمتع العميل بملكية كاملة للبيانات والمتجر
            </div>
            <div class="under-line"></div>
        </div>
        <div class="questions" id="q-2">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                هل يمكنني الحصول على بيانات المتجر بعد انتهاء السنة الأولى؟
            </div>
            <div class="answer">
                نعم، يمكنك امتلاك متجرك بشكل كامل والحصول على بياناته بعد انقضاء السنة الأولى
            </div>
            <div class="under-line"></div>
        </div>

        <div class="questions" id="q-3">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                ما هي الالتزامات المادية إذا اخترت الاستمرار في العمل معكم بعد انقضاء السنة الأولى؟
            </div>
            <div class="answer">
                يمكنك الاستمرار معنا، وتكون قد انتهيت من التزاماتك المادية اتجاه المنصة من حيث أقساط المتجر، ونجدد لك
                الدومين مجاناً، ولا يتوجب إلا دفع تكلفة الحصول على دعم فني إذا اخترت الحصول عليه والذي يكون قد قُدّم
                مجاناً
                طوال السنة الأولى، بالإضافة إلى تكلفة الاستضافة على سيرفرات المنصة.
            </div>
            <div class="under-line"></div>
        </div>

        <div class="questions" id="q-4">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                ماهي تكلفة الاستضافة على سيرفرات منصّة روّاد؟
            </div>
            <div class="answer">1.
                باقة المبتدئين: (موقع واحد، عدد زوار 50 ألف زائر، مساحة الهارد 10 غيغابايت) *** السعر (39 درهم شهرياً)
                <br>
                2.
                باقة التجار: (موقع واحد، عدد زوار 200 ألف زائر، مساحة الهارد 50 غيغابايت) *** السعر (59 درهم شهرياً)
                <br>3.
                باقة الروّاد: (موقع واحد، عدد زوار 500 ألف زائر، مساحة الهارد 100 غيغابايت) *** السعر (99 درهم شهرياً)
            </div>
            <div class="under-line"></div>
        </div>

        <div class="questions" id="q-5">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                أين يقع مقر شركتكم؟
            </div>
            <div class="answer">
                الإمارات العربية المتحدة، دبي، الخليج التجاري، برج تماني آرت.
            </div>
            <div class="under-line"></div>
        </div>

        <div class="questions" id="q-6">
            <div class="question">
                <div class="open-question question-open">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/open.png">

                </div>
                <div class="close-question question-close">
                    <img class="question-img " src="<?= site_url() ?>/wp-content/uploads/2023/01/close.png">

                </div>
                ماهي أفضل بوابات الدفع وشركات الشحن التي تتعاملون معها؟
            </div>
            <div class="answer">
                من خلال منصتنا المؤتمتة ستتعرف على العديد من الخيارات المتاحة للربط قم بإنشاء متجرك الإلكتروني مجاناً
                الآن
                وابدأ الاستمتاع بأفضل الخيارات المتاحة.

            </div>
            <div class="under-line"></div>
        </div>


        <style>
            [class*=" eicon-"], [class^=eicon] {
                display: inline-block;
                font-family: eicons !important;
            }

            .fa, .fas {
                font-family: "Font Awesome 5 Free" !important;
                font-weight: 900;
            }

            .fab {
                font-family: "Font Awesome 5 Brands" !important;
                font-weight: 400;
            }

            .question-close {
                display: none;
            }

            .open-question {
                cursor: pointer;
            }

            .question-img {
                width: 15px;
                margin: 4px;
            }

            .seven-section {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                padding-bottom: 20px;

            }

            .s7-title {
                font-size: 30px;
                margin-bottom: 20px;
                margin-top: 30px;
                font-weight: bolder;

            }

            .question {
                cursor: pointer;
                display: flex;
                width: 100%;
                margin-bottom: 16px;
                /*font-family: "URW DIN Arabic Medium";*/
                font-size: 16px;
                font-weight: 500;
                text-decoration: none;
                color: #000000;
                margin-top: 5px;
            }

            .answer {
                display: none;
                width: 100%;

                font-size: 14px;
                font-weight: 500;
                text-decoration: none;
                color: #000000;
                padding: 15px;
                margin-bottom: 20px;
                line-height: 2;
                margin-right: 5px;
            }

            .questions {
                width: 90%;
            }

            .under-line {
                width: 100%;
                height: 1px;
                border-bottom: 1px solid #d4d4d4;
            }

            .carousel-control.left {
                background: unset !important;
            }

            .carousel-control.right {
                background: unset !important;

            }
        </style>
    </section>
    <style>


        .carousel-indicators {
            bottom: -30px;
        }

        .carousel-indicators li {
            display: inline-block;
            width: 10px;
            height: 10px;
            margin: 1px;
            text-indent: -999px;
            cursor: pointer;
            background-color: #ffffff;
            background-color: #ffffff;
            border: 1px solid #000000;
            border-radius: 10px;
        }

        .carousel-control {
            opacity: 1;
            width: 25px;
            color: #1152f2;
            margin: auto;
        }

        .carousel-control:focus, .carousel-control:hover {
            color: #4bc8f5;
        }

        carousel-control span {
            border: 3px solid #1152f2;
        }

        .carousel-inner {
            margin: auto;
        }

        .carousel-indicators .active {
            background-color: #000000;
        }

        @media only screen and  (min-width: 1024px) {
            .mobile-view {
                display: none !important;
            }

            .tab-view {
                display: none !important;
            }
        }

        @media only screen and (min-width: 1024px) and (max-width: 1199px) {
            .six-section {
                min-height: 420px;
            }

        }

        @media only screen and  (min-width: 1199px) {
            .six-section {
                min-height: 500px;
            }

        }

        @media only screen and (max-width: 1024px) and (min-width: 776px) {
            .desktop-view {
                display: none !important;
            }

            .mobile-view {
                display: none !important;
            }

            .six-section {
                min-height: 500px;
            }

        }


        @media only screen and (min-width: 776px) {
            .left-side {
                width: 60%;
            }

            .second-section {
                margin-top: -60px !important;
            }
        }

        @media only screen and (max-width: 776px) {
            .desktop-view {
                display: none !important;
            }

            .tab-view {
                display: none !important;
            }
        }

        .glyphicon {
            font-family: "Glyphicons Halflings" !important;
        }

        .go-feature {
            font-size: 18px;
            background-color: #4bc8f5 !important;
            color: #FFFFFF !important;
            width: 202px;
            height: 63px;
            display: flex;
            text-align: center;
            align-items: center;
            justify-content: center;
            border-radius: 10px;
        }

        .go-feature:hover {
            background-color: #1152f2 !important;

        }
        .open-question,.close-question{
            margin-left:10px;
        }
        .go-section {
            display: flex;
            align-content: center;
            justify-content: center;
            margin: 55px 10px;
        }




        .go-feature {
            margin: 55px 10px;

        }

        .item {
            padding: 7px;
        }

        .six-section {
            margin-top: 20px;
            margin-bottom: 0px;
            line-height: 1.7;

        }
        .carousel-inner{
            width:95%;
        }

        .mobile {
            display: none;
        }

        .arow-img {

            width: 12px;
        }

        .offer-icon img {
            width: 60px;
        }

        .s3-features {
            width: 90%;
            margin: auto;
        }

        .f-image {
            margin-buttom: 10px;
        }

        .s3-f1-s2 img {
            max-width: 500px;
            width: 100%;
        }

        .s2-tools {
            margin-bottom: 50px;
            margin-top: 40px !important;
        }

        @media only screen and (max-width: 1110px) and (min-width: 776px) {
            .description {
                min-width: 90% !important;
            }
        }

        @media only screen and (max-width: 992px) and (min-width: 776px) {
            .first-section {
                margin: 25px !important;
            }

            .s3-features {
                width: 90%;
                margin: auto;
            }

            .s3-f1-button {
                margin: 10px !important;
            }


        }


        @media only screen and (max-width: 500px) {
            .sub-cart {
                width: 70% !important;
                min-width: 90% !important;
            }
        }

        @media only screen and (max-width: 776px) and (min-width: 500px) {
            .sub-cart {
                width: 70% !important;
                min-width: 400px !important;
            }

        }

        @media only screen and (max-width: 776px) {
            .sub-carts {
                flex-direction: column;

            }


            .th-section {
                margin: auto;
                width: 90%;
                margin-top: 75px;
                margin-bottom: 90px;
            }

            .second-section {
                width: 90% !important;
                margin: auto !important;
            }

            .s4-s2 {
                flex-direction: column;
            }

            .first-section {
                flex-direction: column-reverse;
            }

            .right-side {

                width: 100% !important;
                text-align: center;
            }

            .s1-rs-tt {
                display: flex;
                justify-content: center !important;
            }

            .s3-f1 {
                flex-direction: column;
            }

            .s3-f1-s1 {
                /* width: 41%; */
                width: 100% !important;
                text-align: center;
                display: flex;
                flex-direction: column;
                align-content: center;
                align-items: center;
            }

            .s3-f1-s2 {
                width: 100%;
                margin: auto;
                margin-top: 20px;
            }

            .s3-f-button {
                justify-content: center !important;
                margin-top: 60px !important;

            }

            .s3-f1-s2 {
                display: flex;
                justify-content: center;
                align-content: center;
                align-items: center;
                flex-direction: column;
            }
        }

        .s3-f1-s1-t2 {
            color: #000000 !important;
        }

        .s2-tools {
            WIDTH: 90%;
            margin: auto;
        }

        s2-tools img {
            margin-top: -26px;

        }

        .s3-f1-s1-t2 {
            font-weight: 500;
            /*font-family: "URW DIN Arabic Medium";*/

        }

    </style>
    <style>
        #myCarousel {

        }

        .s6-title {
            text-align: center;
            font-size: 30px;
            font-weight: bolder;
        }

        .s6-title2 {
            color: #282727;
            text-align: center;
            font-size: 18px;
            font-weight: 500;
        }

    </style>

    <style>
        .fi-section {
            margin-top: 50px;
        }

        .s3-f1-s1 {
            width: 41%;
        }

        .item img {
            margin-left: 5px;
            border-radius: 10px;
            width: 100%;
            height: auto;
        }

        .sub-carts {
            display: flex;
            justify-content: center;

        }

        .description {
            /*font-family: "URW DIN Arabic Medium" !important;*/

            text-align: right;
            font-size: 16px;
            font-weight: 500;
            color: #000000;
            width: 55%;
            min-width: 230px;
            margin: auto;
            margin-bottom: 10px;

        }

        .to-checkout {
            color: #ecf1ff;
            width: 126px;
            height: 50px;
            margin: auto;
            display: flex;
            flex-wrap: wrap;
            align-content: center;
            justify-content: center;
            background-color: #1152f2;
            border-radius: 10px;
        }

        .offer-name {
            color: #000000;
            font-family: "Urw Arabic", Sans-serif;
            font-size: 30px;
            font-weight: bold;
        }

        .offer-price {
            color: #1152f2;
            font-size: 25px;
            font-weight: bold;
            margin-bottom: 20px;

        }

        .fi-section {
            width: 90%;
            margin: auto;
        }

        .sub-cart {
            line-height: 2;

            width: 24%;
            margin: 15px auto;
            padding-bottom: 15px;
            border-radius: 10px;
            transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
            box-shadow: 0px 3px 4px -1px rgb(0 0 0 / 50%);

            text-align: center;
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
            align-content: center;
            justify-content: center;
        }

        .subscription-type {
            display: flex;
            justify-content: center;
        }

        .sub-name {
            font-weight: bolder;
            cursor: pointer;
            width: 110px;
            height: 60px;
            border-radius: 10px;
            margin: 10px;
            display: flex;
            justify-content: center;
            align-content: center;
            flex-wrap: wrap;
        }

        .s5-t1 {
            text-align: center;
            font-size: 30px;
            font-weight: bolder;
            color: #000000;
            margin-bottom: 20px;

        }

        .s5-t2 {
            text-align: center;
            font-size: 18px;
            font-weight: 500;
            color: #000000;
            margin-bottom: 22px;

        }

        .s4-s2 {
            margin-top: 50px;
            display: flex;
            justify-content: space-evenly;

        }

        .s4-s2-cart div {
            margin-bottom: 20px;
        }

        .s4-s2-cart {
            display: flex;
            flex-direction: column;
        }

        .cart-img {
            display: flex;
            justify-content: center;
        }

        .cart-t1 {
            color: #000000;
            font-size: 18px;
            font-weight: bold;
        }

        .cart-t2 {
            /*font-family: "URW DIN Arabic Medium";*/

            font-size: 16px;
            font-weight: 500;
            color: #000000;
            line-height: 2.1;
            margin-top: -10px;

        }

        .cart-title {
            text-align: center;
        }

        .first-section {
            display: flex;
            justify-content: center;
            margin: auto;

        }

        .right-side {
            display: flex;
            flex-direction: column;
            justify-content: center;
            width: 50%;

        }


        .new-store-button {
            margin-right: 2px !important;
            font-size: 18px;
            font-weight: 500;
            background-color: #1152f2;
            border-radius: 10px;
            width: 225px;
            height: 55px;
            display: flex;
            align-content: center;
            align-items: center;
            color: #FFFFFF;
            justify-content: center;

        }

        a:focus, a:hover {
            text-decoration: none;
        }


        .s1-rs-ft {
            color: #1152F2;
            font-size: 30px;
            font-weight: bold;
            line-height: 1.7em;
            -webkit-text-stroke-color: #000;
            stroke: #000;
        }

        .s1-rs-st {
            /*font-family: "URW DIN Arabic Medium";*/

            border: 0;
            line-height: 2;
            font-size: 18px !important;
            font-style: inherit;
            font-weight: 500;
            margin: 0;
            outline: 0;
            padding: 0;
            vertical-align: baseline;
            color: #000000;
            --widgets-spacing: 20px;

        }

        .s1-rs-tt {
            display: flex;
            justify-content: flex-start;
        }

        .right-side div {
            margin: 10px;
        }

        section.second-section div {
            text-align: center;

            /*margin-top: 18px !important;*/
        }

        .second-section {
            width: 90%;
            margin: auto;
            display: flex;
            flex-direction: column;
            margin-top: 30px;
        }

        .to-checkout :hover {
            color: #FFFFFF;
            background-color: #4bc8f5;
        }

        .to-checkout a {
            color: #FFFFFF;
            font-weight: bolder;
            width: 100%;
            height: 100%;
            border-radius: 10px;
            display: flex;
            flex-wrap: wrap;
            align-content: center;
            justify-content: center;

        }

        .to-checkout {
            cursor: pointer;
        }


        .s2-ft {
            margin-top: 62px;

            color: #000000;
            font-size: 30px;
            font-weight: bold;
        }

        .s2-st {
            margin-top: 17px;
            /*font-family: "URW DIN Arabic Medium";*/

        }

        .s3-ft {
            width: 90%;
        }

        .s2-st {
            text-align: center;
            color: #000000;
            font-size: 18px;
            font-weight: 500;
        }

        .s3-f1-s1-t1 {
            font-size: 25px;
            font-weight: bold;
            color: #1152F2;
            margin-bottom: 15px;

        }

        .s3-f1-s1-t2 {
            max-width: 550px;
        }

        .s3-f1 {
            display: flex;
            justify-content: space-between;
            max-width: 1170px;
            margin: auto;
        }

        .s3-f1-s2 {
            max-width: 560px;
        }

        .th-section {
            margin-top: 75px;
            margin-bottom: 90px;
        }

        .f-image {
            width: 46px;
        }

        .s3-f1-button {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin: 20px;
            opacity: 0.5;
            cursor: pointer;
        }

        .s3-f-button {
            display: flex;
            justify-content: flex-start;
            max-width: 1170px;
            margin: auto;
            margin-top: -180px;
        }

        .f-t {
            color: black;
            font-weight: bold;
            font-size: 13px;

        }

        .s3-features {
            height: 400px;

        }

        .fo-section {
            margin-top: 30px;
        }

        .s4-title {
            color: #000000;
            font-size: 30px;
            font-weight: bold;
            text-align: center;

        }
    </style>
    <script>


        jQuery(".s3-f1").hide();
        jQuery(".sub-carts").hide();
        jQuery(".f1").show();
        jQuery(".sub-month").show();
        jQuery(".f1").css("opacity", "1");
        jQuery(".sub-month-name").css("background-color", "#1152f2");
        jQuery(".sub-month-name").css("color", "white");

        jQuery(".s3-f1-button").on("click", function (event) {
            const text = event.target.closest(".s3-f1-button").className;
            classname = text.split(" ");
            console.log(classname)
            jQuery(".s3-f1-button").removeAttr("style")
            jQuery(event.target.closest(".s3-f1-button")).css("opacity", "1");
            jQuery(".s3-f1").hide();
            jQuery("." + classname[1]).show()
        });

        jQuery(".question").on("click", function (event) {
            const element = event.target.closest(".questions").getAttribute('id');


            if (jQuery("#" + element).find(".answer").is(":visible")) {

                jQuery("#" + element).find(".answer").slideUp("slow")
                jQuery("#" + element).find(".question-close").hide()
                jQuery("#" + element).find(".question-open").show()

            } else {
                jQuery("#" + element).find(".answer").slideDown("slow");
                jQuery("#" + element).find(".question-close").show()
                jQuery("#" + element).find(".question-open").hide()

            }
        });


        jQuery(".sub-name").on("click", function (event) {
            const text = event.target.closest(".sub-name").className;
            classname = text.split(" ");
            console.log(classname)
            jQuery(".sub-name").removeAttr("style")
            jQuery(event.target.closest(".sub-name")).css("background-color", "#1152f2");
            ;
            jQuery(event.target.closest(".sub-name")).css("color", "white");
            ;
            jQuery(".sub-carts").hide();
            jQuery("." + classname[1]).show()
        });

    </script>
    <?php
} else {
    ?>
    <?php astra_entry_before(); ?>
    <article
        <?php
        echo astra_attr(
            'article-page',
            array(
                'id' => 'post-' . get_the_id(),
                'class' => join(' ', get_post_class()),
            )
        );
        ?>
    >
        <?php astra_entry_top(); ?>
        <header class="entry-header <?php astra_entry_header_class(); ?>">
            <?php astra_get_post_thumbnail(); ?>

            <?php
            astra_the_title(
                '<h1 class="entry-title" ' . astra_attr(
                    'article-title-content-page',
                    array(
                        'class' => '',
                    )
                ) . '>',
                '</h1>'
            );
            ?>
        </header><!-- .entry-header -->

        <div class="entry-content clear"
            <?php
            echo astra_attr(
                'article-entry-content-page',
                array(
                    'class' => '',
                )
            );
            ?>
        >

            <?php astra_entry_content_before(); ?>

            <?php the_content(); ?>

            <?php astra_entry_content_after(); ?>

            <?php
            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . esc_html(astra_default_strings('string-single-page-links-before', false)),
                    'after' => '</div>',
                    'link_before' => '<span class="page-link">',
                    'link_after' => '</span>',
                )
            );
            ?>

        </div><!-- .entry-content .clear -->

        <?php
        astra_edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                esc_html__('Edit %s', 'astra'),
                the_title('<span class="screen-reader-text">"', '"</span>', false)
            ),
            '<footer class="entry-footer"><span class="edit-link">',
            '</span></footer><!-- .entry-footer -->'
        );
        ?>

        <?php astra_entry_bottom(); ?>

    </article><!-- #post-## -->

    <?php astra_entry_after(); ?>
<?php } ?>