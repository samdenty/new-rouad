<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; 

class Widget_TMEA_Instagram_Feed extends Widget_Base {

	public function get_name() {
		return 'tme-instagram_feed';
	}

	public function get_title() {
		return "[TM] " . esc_html__( 'Instagram Feed', 'theme-masters-elementor' );
	}

	public function get_categories() {
		return [ 'theme-masters-elementor' ];
	}

	public function get_style_depends(){
		return [ 'elementor-icons-fa-brands','tme-instagram_feed' ];
	}
    
    public function get_script_depends() {
		return [ 'tme-instagram_feed' ];
	}

	public function get_icon() {
		return 'eicon-instagram-gallery';
	}
    
	protected function _register_controls() {

        // section start
		$this->start_controls_section(
			'settings_section',
			[
				'label' => esc_html__( 'Instagram Feed', 'theme-masters-elementor' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
        
        $this->add_control(
			'source',
			[
                'label' => esc_html__( 'Source', 'theme-masters-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'username' => esc_html__( 'Username', 'theme-masters-elementor' ),
                    'tag' => esc_html__( 'Tag', 'theme-masters-elementor' )
				],
                'default' => 'username'
			]
        );

        $this->add_control(
			'username',
			[
				'label' => esc_html__( 'Username', 'theme-masters-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' =>  'Instagram',
                'condition' => ['source' => 'username']
			]
        );

        $this->add_control(
			'tag',
			[
				'label' => esc_html__( 'Tag', 'theme-masters-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' =>  'paradise',
                'condition' => ['source' => 'tag']
			]
        );

        $this->add_control(
			'image_size',
			[
                'label' => esc_html__( 'Image Size', 'theme-masters-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					640 => esc_html__( '640px', 'theme-masters-elementor' ),
                    480 => esc_html__( '480px', 'theme-masters-elementor' ),
                    320 => esc_html__( '320px', 'theme-masters-elementor' ),
                    240 => esc_html__( '240px', 'theme-masters-elementor' ),
                    150 => esc_html__( '150px', 'theme-masters-elementor' ),
				],
                'default' => 640
			]
        );
        
        $this->add_control(
			'items',
			[
                'label' => esc_html__( 'Number of items to display', 'theme-masters-elementor' ),
                'description' => esc_html__( 'Up to 12 for users, up to 72 for tags.', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 72,
				'step' => 1,
				'default' => 8
			]
        );
        
        $this->add_control(
			'lazy_load',
			[
				'label' => esc_html__( 'Lazy Load', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'theme-masters-elementor' ),
				'label_off' => esc_html__( 'No', 'theme-masters-elementor' ),
				'return_value' => 'yes',
                'default' => 'yes'
			]
        );

        $this->add_control(
			'igtv',
			[
                'label' => esc_html__( 'IGTV', 'theme-masters-elementor' ),
                'description' => esc_html__( 'Display the IGTV feed if available. Only for users.', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'theme-masters-elementor' ),
				'label_off' => esc_html__( 'No', 'theme-masters-elementor' ),
				'return_value' => 'yes',
                'default' => ''
			]
		);
		
		$this->add_control(
			'badge',
			[
                'label' => esc_html__( 'Badge', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'theme-masters-elementor' ),
				'label_off' => esc_html__( 'No', 'theme-masters-elementor' ),
				'return_value' => 'yes',
                'default' => ''
			]
		);
		
		$this->add_control(
			'badge_text',
			[
				'label' => esc_html__( 'Text', 'theme-masters-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' =>  esc_html__( 'INSTAGRAM', 'theme-masters-elementor' ),
                'condition' => ['badge' => 'yes']
			]
		);
		
		$this->add_control(
			'badge_link',
			[
				'label' => esc_html__( 'Link', 'theme-masters-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'condition' => ['badge' => 'yes']
			]
        );
        
        $this->end_controls_section();

        $this->start_controls_section(
			'grid_section',
			[
				'label' => esc_html__( 'Grid Settings', 'theme-masters-elementor' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_responsive_control(
			'columns',
			[
				'label' => esc_html__( 'Columns', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 12,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 4,
				],
				'selectors' => [
                    '{{WRAPPER}} .tmea-instagram-feed .instagram_gallery' => 'grid-template-columns:repeat({{SIZE}}, 1fr);',
                    '{{WRAPPER}} .tmea-instagram-feed .instagram_igtv' => 'grid-template-columns:repeat({{SIZE}}, 1fr);'
				],
			]
        );
        
        $this->add_responsive_control(
			'gap',
			[
				'label' => esc_html__( 'Grid Gap (px)', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
                    '{{WRAPPER}} .tmea-instagram-feed .instagram_gallery' => 'grid-gap: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .tmea-instagram-feed .instagram_igtv' => 'grid-gap: {{SIZE}}{{UNIT}};'
				],
			]
		);
        
        $this->end_controls_section();

         // section start
         $this->start_controls_section(
			'section_thumbnail',
			[
				'label' => esc_html__( 'Image', 'theme-masters-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );
        
        $this->start_controls_tabs( 'tabs_thumbnail_style' );
        
        $this->start_controls_tab(
			'tab_thumbnail_normal',
			[
				'label' => esc_html__( 'Normal', 'theme-masters-elementor' ),
			]
		);
        
        $this->add_responsive_control(
			'thumbnail_opacity',
			[
				'label' => esc_html__( 'Opacity', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 0,
				'max' => 1,
				'step' => 0.1,
				'default' => 1,
                'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed img' => 'opacity: {{VALUE}};'
				],
			]
		);

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'thumb_border',
				'label' => esc_html__( 'Border', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed img',
			]
		);
        
        $this->add_responsive_control(
			'thumb_radius',
			[
				'label' => esc_html__( 'Border Radius', 'theme-masters-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'rem' ],
				'selectors' => [
                    '{{WRAPPER}} .tmea-instagram-feed img' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .tmea-instagram-feed a' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};'
				],
			]
		);
        
        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'thumb_shadow',
				'label' => esc_html__( 'Box Shadow', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed img',
			]
		);
		
		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_css_filter',
				'label' => esc_html__( 'CSS Filters', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed img'
			]
		);
        
        $this->end_controls_tab();

		$this->start_controls_tab(
			'tab_thumbnail_hover',
			[
				'label' => esc_html__( 'Hover', 'theme-masters-elementor' ),
			]
		);
        
        $this->add_responsive_control(
			'thumbnail_opacity_hover',
			[
				'label' => esc_html__( 'Opacity', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 0,
				'max' => 1,
				'step' => 0.1,
				'default' => 1,
                'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed a:hover img' => 'opacity: {{VALUE}};'
				],
			]
		);
        
        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'thumb_border_hover',
				'label' => esc_html__( 'Border', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed a:hover img',
			]
		);
        
        $this->add_responsive_control(
			'thumb_radius_hover',
			[
				'label' => esc_html__( 'Border Radius', 'theme-masters-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'rem' ],
				'selectors' => [
                    '{{WRAPPER}} .tmea-instagram-feed a:hover img' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .tmea-instagram-feed a:hover' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};'
				],
			]
		);
        
        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'thumb_shadow_hover',
				'label' => esc_html__( 'Box Shadow', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed a:hover img',
			]
		);
		
		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'thumbnail_css_filter_hover',
				'label' => esc_html__( 'CSS Filters', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed a:hover img'
			]
		);

        $this->end_controls_tab();
		$this->end_controls_tabs();
		
		$this->end_controls_section();

         // section start
         $this->start_controls_section(
			'section_badge_style',
			[
				'label' => esc_html__( 'Badge', 'theme-masters-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => ['badge' => 'yes']
			]
		);
		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'badge_typography',
				
				'selector' => '{{WRAPPER}} .tmea-instagram-feed-badge a',
			]
		);

		$this->add_control(
			'badge_hr_1',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);  

		$this->start_controls_tabs( 'tabs_button_style' );
        
        $this->start_controls_tab(
			'tab_button_normal',
			[
				'label' => esc_html__( 'Normal', 'theme-masters-elementor' ),
			]
		);
        
        $this->add_control(
			'badge_text_color',
			[
				'label' => esc_html__( 'Text Color', 'theme-masters-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a' => 'color: {{VALUE}};',
				]
			]
		);
        
        $this->add_control(
			'badge_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'theme-masters-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a' => 'background-color: {{VALUE}};',
				]
			]
		);
        
        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'badge_border',
				'label' => esc_html__( 'Border', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed-badge a'
			]
		);
        
        $this->add_responsive_control(
			'badge_border_radius',
			[
				'label' => esc_html__( 'Border Radius', 'theme-masters-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'rem' ],
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};'
				]
			]
		);
        
        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'badge_border_shadow',
				'label' => esc_html__( 'Box Shadow', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed-badge a'
			]
		);
        
        $this->end_controls_tab();

		$this->start_controls_tab(
			'tab_button_hover',
			[
				'label' => esc_html__( 'Hover', 'theme-masters-elementor' ),
			]
		);
        
        $this->add_control(
			'badge_text_hover_color',
			[
				'label' => esc_html__( 'Text Color', 'theme-masters-elementor' ),
				'type' => Controls_Manager::COLOR, 
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a:hover' => 'color: {{VALUE}};'
				]
			]
		);
        
        $this->add_control(
			'badge_bg_hover_color',
			[
				'label' => esc_html__( 'Background Color', 'theme-masters-elementor' ),
				'type' => Controls_Manager::COLOR, 
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a:hover' => 'background-color: {{VALUE}};'
				]
			]
		);
        
        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'badge_hover_border',
				'label' => esc_html__( 'Border', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed-badge a:hover'
			]
		);
        
        $this->add_responsive_control(
			'badge_border_hover_radius',
			[
				'label' => esc_html__( 'Border Radius', 'theme-masters-elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'rem' ],
				'selectors' => [
					'{{WRAPPER}} .tmea-instagram-feed-badge a:hover' => 'border-top-left-radius: {{TOP}}{{UNIT}};border-top-right-radius: {{RIGHT}}{{UNIT}};border-bottom-right-radius: {{BOTTOM}}{{UNIT}};border-bottom-left-radius: {{LEFT}}{{UNIT}};'
				]
			]
		);
        
        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'badge_border_hover_shadow',
				'label' => esc_html__( 'Box Shadow', 'theme-masters-elementor' ),
				'selector' => '{{WRAPPER}} .tmea-instagram-feed-badge a:hover'
			]
		);
        
        $this->end_controls_tab();
		$this->end_controls_tabs();
		
		$this->add_control(
			'badge_hr_2',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		); 
        
        $this->add_responsive_control(
			'badge_padding',
			[
				'label' => esc_html__( 'Padding', 'theme-masters-elementor' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'rem' ],
				'selectors' => [
                    '{{WRAPPER}} .tmea-instagram-feed-badge a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				],
			]
		);
        
		$this->end_controls_section();
  
	}

	/**
	 * Render 
	 */
	protected function render( ) {
		$settings = $this->get_settings_for_display();
        ?>
		<div class="tmea-instagram-feed-container">
			<?php if ($settings['badge']) { ?>
				<div class="tmea-instagram-feed-badge">
					<a href="<?php echo esc_url($settings['badge_link']); ?>" class="<?php if (empty($settings['badge_link'])) { echo 'badge-link-disabled'; } ?>">
						<i class="fab fa-instagram"></i> <?php echo $settings['badge_text']; ?>
					</a>
				</div>
			<?php } ?>
			<div class="tmea-instagram-feed" data-source="<?php echo $settings['source']; ?>" data-username="<?php echo esc_attr($settings['username']); ?>" data-selectedtag="<?php echo esc_attr($settings['tag']); ?>" data-items="<?php echo esc_attr($settings['items']); ?>" data-imgsize="<?php echo esc_attr($settings['image_size']); ?>" data-lload="<?php echo $settings['lazy_load'] ? 'true' : 'false'; ?>" data-igtv="<?php echo $settings['igtv'] ? 'true' : 'false'; ?>">
				<div class="tmea-instagram-feed-loading">Loading...</div>
			</div>
		</div>
	<?php
    } 

}
Plugin::instance()->widgets_manager->register_widget_type( new Widget_TMEA_Instagram_Feed() );