<?php
/**
 * Plugin Name: WHI
 * Description:
 * Version: 1.0.0
 * Author: muhammad
 * Domain Path: /languages
 */
define('ABSPATH', 'Hey , what are you doing here? You silly human!');

if (!defined('ABSPATH')) {
    exit;
}
require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php' );
require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-ssh2.php' );

$filePath = __DIR__ . '/vendor/autoload.php';
if (!file_exists($filePath)) exit;
else require_once $filePath;

// TODO: automate version
define('WHI_VERSION', '1.0.0');
define('PLUGIN_NAME', 'WHI');
define('WHI_PLUGIN_FIRST_TIME', 'WHI_plugin_first_time');
define('WHI_AUTHENTICATE', 'WHI_authenticate_action');
define('WHI_UPDATE_THEME', 'WHI_update_theme');
define('WHI_SET_PLUGIN_STATUS', 'WHI_set_plugin_status');
define('WHI_UPDATE_INDEXING_NUMBER', 'WHI_update_indexing_number');
define('WHI_RESET_PLUGIN', 'WHI_reset_plugin');
define('WHI_FIRST_ACTIVATION', 'WHI_first_activation');
/* set session */
session_start();
require_once __DIR__ . '/src/index.php';
require_once __DIR__ . '/WHI-api.php';
/* end sso */