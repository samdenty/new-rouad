<?php

namespace Sso\WP\Helpers;

use chillerlan\Traits\ArrayHelpers\ByteArray;
use  Sso\WP\Helpers\ApiHelper;

class FunctionApiHelper extends Domain
{
    protected $id;
    protected $api;
    protected $row_id;
    protected $whid;
    private static $instance;

    private function __construct()
    {
        $this->api=CrudApiHelper::getInstance();
        parent::__construct();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new FunctionApiHelper();
        }
        return self::$instance;

    }

    public function createuserDomain($id)
    {
        $this->id = $id;
        $response = $this->CreateDomain();
        if ($response) {
            $bool = $this->storeDomain($response);
            if ($bool) {
                wp_redirect(get_home_url() . "/my-account?domain=" . $_POST["domain"], $status = 302);
            } else {
                wp_redirect(get_home_url() . "/my-account", $status = 302);

            }
        }
        exit();
    }
    public function storeDomain($response)
    {
        $array = array();
        foreach ($response as $results) {
            if ($results["@attributes"]) {
                $array = $results["@attributes"];
                if ($array["Registered"] == true) {
                    $user = get_user_by("ID", $this->id);
                    update_user_meta($user->ID, "serv", 1);;
                    global $wpdb;
                    $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $_POST["domain"] . "' limit 1;";
                    $result = $wpdb->get_results($sql);
                 //   var_dump($result);
                    $row = $result[0]->id;
                    if (!$row) {
                        $sql = "INSERT INTO `" . $wpdb->base_prefix . "domains`(`user_id`,`domain_name`,`steps`) VALUES ('" . $this->id . "','" . $_POST["domain"] . "','" . 0 . "');";
                        $result = $wpdb->get_results($sql);
                  //  var_dump($result);
                  //  var_dump($wpdb->last_error);
                    }
                    $this->row_id = $this->api->AddOrder($this->id);
                    $this->whid = $user->WHID;
                  //  var_dump($wpdb->last_error);
                    return true;
                } else {
                    update_user_meta($this->id, "serv", 0);
                    return false;
                }
            }
        }
    }
    public function createAdminDomain($id)
    {
        $this->id=$id;
        $response = $this->CreateDomain();
        if ($response) {
            $bool = $this->storeDomain($response);
            if ($bool) {
                wp_redirect(get_home_url() . "/wp-admin/admin.php?page=subscriber&id=" . $id . "&WhiId=" . $this->whid . "&row_id=" . $this->row_id, $status = 302);
            } else {
                wp_redirect(get_home_url() . "/my-account", $status = 302);

            }
        }
        exit();
    }
    public function CreateDomain()
    {
        // array_push($this->config, "Command", "namecheap.domains.getList");
        $this->config["Command"] = "namecheap.domains.create";
        $this->config["DomainName"] = $_POST["domain"] ? $_POST["domain"] : "dfxcxaldfalfaf.com";
        $this->config["Years"] = 1;
        $this->config["RegistrantFirstName"] = "muhammad";
        $this->config["RegistrantLastName"] = "muhammad";
        $this->config["RegistrantAddress1"] = "muhammad";
        $this->config["RegistrantCity"] = "Dubai";
        $this->config["RegistrantStateProvince"] = "muhammad";
        $this->config["RegistrantPostalCode"] = "muhammad";
        $this->config["RegistrantCountry"] = "UAE";
        $this->config["RegistrantPhone"] = "+971.509503511";
        $this->config["RegistrantEmailAddress"] = "muhammadqanah.1997@gmail.com";
        $this->config["TechFirstName"] = "muhammad";
        $this->config["TechLastName"] = "muhammad";
        $this->config["TechAddress1"] = "UAE";
        $this->config["TechCity"] = "UAE";
        $this->config["TechStateProvince"] = "UAE";
        $this->config["TechPostalCode"] = "0000";
        $this->config["TechCountry"] = "UAE";
        $this->config["TechPhone"] = "+971.509503511";
        $this->config["TechEmailAddress"] = "muhammadqanah.1997@gmail.com";
        $this->config["AdminFirstName"] = "UAE";
        $this->config["AdminLastName"] = "UAE";
        $this->config["AdminAddress1"] = "UAE";
        $this->config["AdminCity"] = "UAE";
        $this->config["AdminStateProvince"] = "UAE";
        $this->config["AdminPostalCode"] = "UAE";
        $this->config["AdminCountry"] = "UAE";
        $this->config["AdminPhone"] = "+971.509503511";
        $this->config["AdminEmailAddress"] = "muhammadqanah.1997@gmail.com";
        $this->config["AuxBillingFirstName"] = "UAE";
        $this->config["AuxBillingLastName"] = "UAE";
        $this->config["AuxBillingAddress1"] = "UAE";
        $this->config["AuxBillingCity"] = "UAE";
        $this->config["AuxBillingStateProvince"] = "UAE";
        $this->config["AuxBillingPostalCode"] = "UAE";
        $this->config["AuxBillingCountry"] = "UAE";
        $this->config["AuxBillingPhone"] = "+971.509503511";
        $this->config["AuxBillingEmailAddress"] = "muhammadqanah.1997@gmail.com";
        $this->config["Extended attributes	"] = "com";
        $this->config["Nameservers"] = "ns1.rouad.pro,ns2.rouad.pro";
        $this->config["AuxBillingPhone"] = "+971.509503511";
        $response = $this->callApi();
        return $response;
    }
    public function CheckDomain($domainlist)
    {

        if (!$domainlist) {
            $domainlist =  "the-" . $_POST["domains"].",".$_POST["domains"] ;
        }
        $this->config["Command"] = "namecheap.domains.check";
        $this->config["DomainList"] = $domainlist;
        $response = $this->callApi();
        $array = array();
        //var_dump($response);
        foreach ($response as $domains) {
            foreach ($domains as $domain) {
                $array[] = ["domain" => $domain["@attributes"]["Domain"],

                    "value" => $domain["@attributes"]["Available"]];
            }
        }
        $array = json_encode($array);
        die($array);
    }


    public function checkPurchKeySso($conf = array())
    {
        $this->header = $conf;
        $this->content = '';
        $this->callapi($conf);
        return $this->getApi();
    }

    public function SaveAuth($conf = array())
    {
        global $wpdb;
  foreach ($conf as $key=>$value)
  {
      update_option($key, $value);
  }
    }

    public function checkAuth()
    {
        if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
            $user = get_option("WHI_admin_user");
            $pass = get_option('WHI_admin_pass');
            $api = get_option("WHI_admin_api_key");

            return ($user && $pass && $api) ? true : false;
        }
        return false;
    }

    public function checkUserInfoSso()
    {
        $this->url = self::DOMAIN . '/checkUserInfoIsUpdated';
        $this->content = '';
        $result = $this->getApi();

        return $this->getResult($result);
    }

    public function confirmUserInfoSso()
    {
        $this->url = self::DOMAIN . '/confirmUserInfoUpdated';
        $result = $this->postApi();

        return $this->getResult($result);
    }


    public function addOfferSso($arg = array())
    {
        $this->url = self::DOMAIN . '/addNewOffer';
        $this->content = $arg;
        $result = $this->postApi();

        return $this->getResult($result);
    }

    public function getProductPrice($arg = array())
    {
        $this->url = self::DOMAIN . '/getProductPrice';
        $this->content = $arg;
        $result = $this->getApi();

        return $this->getResult($result);
    }

    public function getMembersInfo($page = 1)
    {
        $this->url = self::DOMAIN . '/getMembersInfo?page=' . $page;
        $this->content = '';
        $result = $this->getApi();

        return $this->getResult($result);
    }

    public function getMemberById($id)
    {
        $this->url = self::DOMAIN . '/getMemberInfoById';
        $this->content = array("id" => $id);
        $result = $this->getApi();

        return $this->getResult($result);
    }


    public function CheckPostToShow($arg = array())
    {
        $this->url = self::DOMAIN . '/checkPostIsValidToShow';
        $this->content = $arg;
        $result = $this->getApi();

        return $this->getResult($result);
    }


}


