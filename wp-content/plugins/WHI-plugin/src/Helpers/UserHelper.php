<?php


namespace Sso\WP\Helpers;

class UserHelper
{
    private static $instance;

    private function __construct()
    {
       $this->api = FunctionApiHelper::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new UserHelper();
        }
        return self::$instance;
    }
}