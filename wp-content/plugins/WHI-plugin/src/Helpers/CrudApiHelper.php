<?php


namespace Sso\WP\Helpers;

use Sso\WP\Helpers\InitServer;

use WP_User_Query;

class CrudApiHelper extends WhiHelper
{
    protected static $instance;

    private function __construct()
    {
        Parent::__construct();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new CrudApiHelper();
        }
        return self::$instance;
    }


    public function AddClientApi($user)
    {
        if (!$user)
            $user = wp_get_current_user();
        $check = array(
            'search' => $user->user_email,
            'clientip' => "00.00.00.00",
            'action' => 'GetClients');
        $result = ($this->callapi($check));
        if ($result['clients']) {
            $clients = $result['clients'];
            foreach ($clients as $client) {
                foreach ($client as $array) {
                    if ($array['email'] == $user->user_email) {
                        update_user_meta($user->ID, "WHI_ID", $array["id"]);
                        return 0;
                    }
                }
            }
        } else {
            $conf = array(
                'firstname' => $user->user_login,
                'lastname' => $user->user_login,
                'email' => $user->user_email,
                'address1' => $user->user_login,
                'city' => $user->user_login,
                'postcode' => "00000",
                'country' => "AE",
                'phonenumber' => "000000000",
                'password2' => "testpassword",
                'clientip' => "00.00.00.00",
                'action' => 'AddClient');
            $user_WHI = $this->callapi($conf);
            update_user_meta($user->ID, "WHI_ID", $user_WHI["clientid"]);
        }
        return $user_WHI;
    }

    public function SyncAll()
    {
        $customer_query = new WP_User_Query(
            array(
                'role' => 'customer',
            )
        );
        $users = $customer_query->get_results();
        foreach ($users as $user) {
            $this->AddClientApi($user);
        }
    }

    public function AddOrder($id)
    {
        global $wpdb;
        $user = get_user_by("ID", $id);
        get_user_meta($id, "WHI_ID");
        $domain = $_POST["domain"];
        $products = $this->getUserSubsicrption();
        $product = array(6484 => 1, 7572 => 2, 7219 => 3);
        foreach ($products as $key => $value) {
            if ($product[$value])
                $final[] = $product[$value];
        }
        if (!$user->WHI_ID) {
            ($this->AddClientApi($user));
        }
        $config = array(
            'action' => 'AddOrder',
            'clientid' => $user->WHI_ID ? $user->WHI_ID : 66,
            'pid' => $final[0] ? $final[0] : 1,
            'domain' => $domain,
            'addons' => array('1,3,9', ''),
            'customfields' => array(base64_encode(serialize(array("1" => "Google"))), base64_encode(serialize(array("1" => "Google")))),
            'configoptions' => array(base64_encode(serialize(array("1" => 999))), base64_encode(serialize(array("1" => 999)))),
            'nameserver1' => 'ns1.rouad.pro',
            'nameserver2' => 'ns2.rouad.pro',
            'paymentmethod' => 'banktransfer',
            'responsetype' => 'json');
        $result = $this->callApi($config);

        if ($result["result"] == "success") {
            $row_id = $this->AcceptOrder($result["orderid"], $id);
        }
        return $row_id;
    }

    function AcceptOrder($id, $userid)
    {
        $name = explode(".", $_POST["domain"]);
        $length = 12;
        $keyspace = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz';
        {
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
        }
        echo $a = $str;
        $config = array(
            'serverid' => 2,
            'orderid' => $id,
            'action' => "AcceptOrder",
            'autosetup' => true,
            'serviceusername' => $str,
            'servicepassword' => "pasPas&UYTIOJH",
            'sendemail' => false,
        );
        global $wpdb;
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $_POST["domain"] . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        if (!$row) {
            $sql = "INSERT INTO `" . $wpdb->base_prefix . "domains`(`user_id`,`domain_name`,`username`,`password`,`steps` ,`whOrder_id`) VALUES (" . $userid . ",'" . $_POST["domain"] . "','" . $str . "','pasPas&UYTIOJH',1,'" . $id . "');";
            $result = $wpdb->get_results($sql);
        } else {
            $sql = "update `{$wpdb->base_prefix}domains` set `steps`=1 ,`user_id`='" . $userid . "',`username`='" . $str . "',`password`='pasPas&UYTIOJH' ,`whOrder_id`='" . $id . "'   where  `id`= " . $row;
            $info = $wpdb->get_results($sql);;
        }
        $sql = "SELECT id FROM`" . $wpdb->base_prefix . "domains`where `domain_name`='" . $_POST["domain"] . "' limit 1;";
        $result = $wpdb->get_results($sql);
        $row = $result[0]->id;
        update_user_meta($userid, "serv", 1);
        $result = $this->callApi($config);
        return $row;
    }

    function GetProducts()
    {

        $config = array(
            'action' => "GetProducts",
        );
    }

    function getUserSubsicrption()
    {
        $user = wp_get_current_user();
        $users_subscriptions = wcs_get_users_subscriptions($user->ID);
        foreach ($users_subscriptions as $subscription) {
            if ($subscription->has_status(array('active'))) {
                $items = ($subscription->order->get_items());
                foreach ($items as $item) {
                    $product_ids[] = $item->get_product_id();
                }
            }
        }
        return $product_ids;
    }

    public function installProject()
    {
        global $wpdb;


        $server = new InitServer();
        $server->createDatabase();

        $is_error = empty($wpdb->last_error);
        // var_dump($result);
        die("dfsdfd");
    }

    public function installProjectFile()
    {
        global $wpdb;
        $server = new InitServer();
        $server->createTrialProjectFile();
        $is_error = empty($wpdb->last_error);
        // var_dump($result);
        $user = wp_get_current_user();
//echo $user->TrialDomain;
        die($user->TrialDomain);
    }

    public function installTrial()
    {
        global $wpdb;
        $server = new InitServer();
        $server->createTrialDatabase();
        $is_error = empty($wpdb->last_error);
        // var_dump($result);
        $user = wp_get_current_user();
//echo $user->TrialDomain;
        die($user->TrialDomain);
    }

    function CreeateUserTrial()
    {
        $user = wp_get_current_user();
        if (!$user->trial) {
            $server = new InitServer();
            $server->createTrialDatabase();
        }
        die();
    }
    public function createData()
    {
        global $wpdb;
        $server = new InitServer();
        $server->createData();
        $is_error = empty($wpdb->last_error);
        die("done");
    }
    function setup_1()
    {
            $server = new InitServer();
            $server->setup_1();
        die("done");
    }

    function setup_2()
    {
        $server = new InitServer();
        $server->setup_2();
        die("done");
    }
    function setup_3()
    {
        $server = new InitServer();
        $server->setup_3();
        die("done");
    }
    function setup_4()
    {
        $server = new InitServer();
        $server->setup_4();
        die("done");
    }
    function setup_5()
    {
        $server = new InitServer();
        $server->setup_5();
        die("done");
    }
    function setup_6()
    {
        $server = new InitServer();
        $server->setup_6();
        die("done");
    }
    function setup_7()
    {
        $server = new InitServer();
        $server->setup_7();
        die("done");
    }
    function setup_8()
    {
        $server = new InitServer();
        $server->setup_8();
        die("done");
    }
}