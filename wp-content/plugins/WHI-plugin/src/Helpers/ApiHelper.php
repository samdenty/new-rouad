<?php


namespace Sso\WP\Helpers;


use WPMailSMTP\Vendor\phpseclib3\Crypt\EC\Curves\brainpoolP160r1;

class ApiHelper
{
    // const DOMAIN = 'https://web.rouad.com/includes/api.php';
    protected $Baseurl;
    protected $header = array();
    protected $content = array();
    protected $args = array();
    protected $config = array();

    protected function getApi()
    {
        $stuff_response = wp_remote_get($this->url, $this->args);
     //   var_dump($stuff_response["headers"]);
        if (is_array($stuff_response)) {
            $body = json_decode($stuff_response['body']); // use the content
            //   var_dump($body);
            return $body;
        }
    }

    protected function callApi()
    {
        //var_dump(json_encode($this->config));

        //   var_dump($this->Baseurl);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->Baseurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->config));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $headers = array();
        //$headers[] = 'Content-Type: application/json';
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $headers = array(
            "Accept: application/json",
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close($ch);
        $res = json_decode($response, true);
     // var_dump($response);
        if (!$res) {
            if(simplexml_load_string($response)) {
                $xml = simplexml_load_string($response);
                $json = json_encode($xml);
                $array = json_decode($json, TRUE);
                $response = $array['CommandResponse'];
                // var_dump($xml);
            }
            return $response;
        }
     //     var_dump($res);
        return $res;
    }

    protected function getResult($result)
    {

        if (isset($result->result) && ($result->result == "success")) return $result;
        elseif (isset($result->message) && !empty($result->message)) return $result->message;

        return false;
    }

}