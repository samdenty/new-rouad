<?php

use Sso\WP\Helpers\FunctionApiHelper;

if ($_POST) {
    $array = [
        'action' => $_POST['action'],
        'WHI_admin_user' => $_POST['WHI_admin_user'],
        'WHI_admin_pass' => $_POST['WHI_admin_pass'],

        'WHI_admin_api_key' => $_POST['WHI_admin_api_key'],
        'WHI_admin_url' => $_POST['WHI_admin_url'],
        'serverip' => $_POST['serverip'],
        'serverUserName' => $_POST['serverUserName'],
        'serverPass' => $_POST['serverPass'],
        'gitlabRepo' => $_POST['gitlabRepo'],
        'gitlabUserName' => $_POST['gitlabUserName'],
        'gitlabPass' => $_POST['gitlabPass']




    ];
    $WHI_plugin = FunctionApiHelper::getInstance();
    $WHI_plugin->SaveAuth($array);
}
?>
  <?php
$api_test = true;
$helper_test = true;
?>
<form method="post" action="<?= esc_url($_SERVER['REQUEST_URI']) ?>">

<table style="width: 100%;    height: 400px;
">
    <tr>
        <td>                    <?php $field = "WHI_admin_url"; ?>
            <label for="whmcs_admin_url">
                <?php echo esc_html_x('api url   ', "admin", 'whcom') ?>
            </label>
        </td>
        <td>
            <input id="whmcs_admin_url" type="url" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'WHI_admin_user' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('API Identifier', "admin", 'whcom') . "</strong>";
                ?>
            </label>

        </td>
        <td>            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                               value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'WHI_admin_pass' ?>
            <label for="<?php echo "$field" ?>"><?php
                echo "<strong>" . esc_html_x('API Secret', "admin", 'whcom') . "</strong>";
                ?>
            </label>

        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="password" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'WHI_admin_api_key' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('API Key', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'serverip' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('server ip', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'serverUserName' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('server user name', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'serverPass' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('server password ', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="password" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>


    <tr>
        <td>
            <?php $field = 'gitlabRepo' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('gitlab url', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'gitlabUserName' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('gitlab user name', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'gitlabPass' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('gitlab password', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="password" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>
    <tr>
        <td>
            <?php $field = 'nameCheapUrl' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('name cheap  url', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>

    <tr>
        <td>
            <?php $field = 'nameCheapApiKey' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('nameCheapApiKey', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>
    <tr>
        <td>
            <?php $field = 'nameCheapApiUser' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('nameCheapApiUser', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>
    <tr>
        <td>
            <?php $field = 'nameCheapUserName' ?>
            <label for="<?php echo "$field" ?>">
                <?php
                echo "<strong>" . esc_html_x('nameCheapUserName', "admin", 'whcom') . "</strong>";
                ?>
            </label>
        </td>
        <td>

            <input id="<?php echo "$field" ?>" type="text" name="<?php echo "$field" ?>"
                   value="<?php echo esc_attr(get_option($field)); ?>">
        </td>
    </tr>
    <tr>
    <td></td>
    <td>                <div class="whcom_text_center">
            <button type="submit"
                    class="whcom_button"><?php echo esc_html_x('Save Settings', "admin", 'whcom') ?></button>
        </div>
    </td>
</tr>

</table>

</form>

<!--    <div class="wrap whcom_main">-->
<!--    <form method="post" action="--><?//= esc_url($_SERVER['REQUEST_URI']) ?><!--">-->
<!--        --><?php //settings_fields('WHI'); ?>
<!--        <input type="hidden" name="action" value="AddClient">-->
<!---->
<!--        <div class="whcom_panel">-->
<!--            <div class="whcom_panel_header whcom_panel_header_white">-->
<!--                <span>--><?php //echo esc_html_x(' Authentication Credentialst   ', "admin", 'whcom') ?><!--</span>-->
<!--            </div>-->
<!--            <div class="whcom_panel_body">-->
<!---->
<!--                <div class="whcom_form_field whcom_form_field_horizontal">-->
<!--                    --><?php //$field = "WHI_admin_url"; ?>
<!--                    <label for="whmcs_admin_url">-->
<!--                        --><?php //echo esc_html_x('WHMCS Frontend URL   ', "admin", 'whcom') ?>
<!---->
<!--                    </label>-->
<!--                    <input id="whmcs_admin_url" type="url" name="--><?php //echo "$field" ?><!--"-->
<!--                           value="--><?php //echo esc_attr(get_option($field)); ?><!--">-->
<!--                    <label></label>-->
<!--                    <small class="whcom_url_click_img" style="cursor: pointer"> <i> <u>--><?php //echo esc_html_x('Where do I find this?', "admin", 'whcom') ?><!--</u></i> </small>-->
<!--                </div>-->
<!--                <div class="whcom_form_field whcom_form_field_horizontal whcom_text_right whcom_image_url_show"-->
<!--                     style="display: none">-->
<!--                    <img src="--><?php //echo WHCOM_URL . '/admin/assets/images/client-system-url.png' ?><!--" alt=""-->
<!--                         style="width: 65%">-->
<!--                </div>-->
<!---->
<!--                <div class="whcom_form_field whcom_form_field_horizontal">-->
<!--                    --><?php //$field = 'WHI_admin_user' ?>
<!--                    <label for="--><?php //echo "$field" ?><!--">-->
<!--                        --><?php
//                        echo "<strong>" . esc_html_x('WHMCS API Identifier', "admin", 'whcom') . "</strong>";
//                        echo "<br>";
//                        echo esc_html_x('(or you can also use your WHMCS admin user name)', "admin", 'whcom');
//                        ?>
<!--                    </label>-->
<!--                    <input id="--><?php //echo "$field" ?><!--" type="text" name="--><?php //echo "$field" ?><!--"-->
<!--                           value="--><?php //echo esc_attr(get_option($field)); ?><!--">-->
<!--                </div>-->
<!---->
<!--                <div class="whcom_form_field whcom_form_field_horizontal">-->
<!--                    --><?php //$field = 'WHI_admin_pass' ?>
<!--                    <label for="--><?php //echo "$field" ?><!--">--><?php
//                        echo "<strong>" . esc_html_x('WHMCS API Secret', "admin", 'whcom') . "</strong>";
//                        echo "<br>";
//                        echo esc_html_x('(or your admin password)', "admin", 'whcom');
//                        ?>
<!--                    </label>-->
<!--                    <input id="--><?php //echo "$field" ?><!--" type="password" name="--><?php //echo "$field" ?><!--"-->
<!--                           value="--><?php //echo esc_attr(get_option($field)); ?><!--">-->
<!--                </div>-->
<!---->
<!--                <div class="whcom_form_field whcom_form_field_horizontal">-->
<!--                    --><?php //$field = 'WHI_admin_api_key' ?>
<!--                    <label for="--><?php //echo "$field" ?><!--">-->
<!--                        --><?php //echo esc_html_x('WHMCS API Key', "admin", 'whcom') ?>
<!--                        <a href="http://docs.whmpress.com/whmcs-cart-order-pages/creating-autoauth-key-in-whmcs/"-->
<!--                           target="_blank"><i class="whcom_icon_help-circled"></i></a>-->
<!--                    </label>-->
<!--                    <input id="--><?php //echo "$field" ?><!--" type="text" name="--><?php //echo "$field" ?><!--"-->
<!--                           value="--><?php //echo esc_attr(get_option($field)); ?><!--">-->
<!--                </div>-->
<!--                <div class="whcom_text_center">-->
<!--                    <button type="submit"-->
<!--                            class="whcom_button">--><?php //echo esc_html_x('Save Settings', "admin", 'whcom') ?><!--</button>-->
<!--                </div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!---->
<!--    </form>-->
<style>
    input{
        width: 80%;
        direction: ltr;
    }
</style>
