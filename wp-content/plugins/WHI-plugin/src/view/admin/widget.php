<div class="container">
    <h2><?= __('show widget', 'sso') ?></h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th><?= __('title', 'sso') ?></th>
            <th><?= __('edit', 'sso') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($data) {
            foreach ($data as $d) { ?>
                <tr>
                    <td class="widget-font"><?= $d->option_name ?></td>
                    <td>
                        <a href="<?= admin_url('admin.php?page=widget&id=' . $d->option_id . '&name=' . $d->option_name) ?>">
                            <button type="button" href="" class="btn btn-primary"><?= __('edit', 'sso') ?></button>
                        </a></td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>