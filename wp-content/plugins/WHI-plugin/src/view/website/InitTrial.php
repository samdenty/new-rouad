<?php

$user = wp_get_current_user();
if (!$user->TrialDomain) {
    ?>
    <style>
        .finish-photo {
            display:none!important;
        }
    </style>
    <?php
}
?>
<div class="body" style="<?php if ($user->TrialDomain) echo "display:none;"; ?>">
    <div class="card">

        <div class="info__padding">
            <p class="second-title-label">
            </p>


            <div class="main-section">
                <div class="section-right">


                    <div class="Trial-photo">
                        <img src="<?= site_url() ?>/wp-content/plugins/WHI-plugin/src/view/website/assets/icons/TrialPhoto.png">

                    </div>
                    <div class="Trial-hint">
                        شاهد الفيديو التعريفي
                    </div>
                </div>


                <div class="section-left">
                    <div class="promo-Trial">


                        يمكنك دخول عالم التجارة الإلكترونية بضغطة زر
                        <br>
                        وبدون أي تعقيدات

                    </div>
                    <div class="store-name-label">ما هو اسم متجرك ؟
                    </div>
                    <div class="store-description">
                        أدخل اسم متجرك بالشكل الذي تريده أن يظهر لعملائك. يمكنك تغيير
                        <br>
                        اسم المتجر الخاص بك في أي وقت لاحق.
                    </div>
                    <div class=" finish-progres">
                        <div class="form-success-title">
                            <?= __('تم اعداد موقعك بنجاح ', 'sso') ?>
                        </div>
                        <div class="form-success">
                            <form method="post" class="TrialSiteUrl" action="https://.rouad.tech" target="_blank">
                                <input type="hidden" name="username" value=<?= $user->user_login ?>>
                                <input type="hidden" name="admin" value=<?= $user->user_email ?>>
                                <input type="hidden" name="password" value=<?= $user->user_pass ?>>
                                <input type="hidden" name="id" value=" <?= $user->ID ?>"> <input class="TrialSiteButton"
                                                                                                 type="submit"
                                                                                                 value="اضغط للدخول إلى ">

                            </form>
                        </div>
                    </div>
                    <div id="myProgress">
                        <img src="<?= site_url() ?>/wp-content/plugins/WHI-plugin/src/view/website/assets/icons/waitingjif.gif">
                        <img src="<?= site_url() ?>/wp-content/plugins/WHI-plugin/src/view/website/assets/icons/waiting.gif">
                        <!--                        <div id="myBar"></div>-->
                    </div>
                    <table class="container" id="form">
                        <tr class="navbar-nav mr-auto">
                            <td>
                                <div class="form-label label">
                                    اسم المتجر باللغة الانكليزية
                                </div>
                            </td>
                        </tr>

                        <tr class="navbar-nav mr-auto">

                            <td>
                                <label>

                                    <input type="text" id="TrialDomain" name="TrialDomain" class="form-input"
                                           value="<?= strtolower($user->TrialDomain) ?>"
                                           required>
                                </label>
                            </td>
                        </tr>

                        <tr class="navbar-nav mr-auto">
                            <td>
                                <div class="form-label label">
                                    اسم المتجر
                                </div>
                            </td>
                        </tr>

                        <tr class="navbar-nav mr-auto">

                            <td>
                                <label>

                                    <input type="text" id="siteName" name="siteName" class="form-input"
                                           value="<?= strtolower($user->user_login) ?>" required>
                                </label>
                            </td>
                        </tr>
                        <!--    <tr>-->
                        <!--        <td>-->
                        <!--            <div class="label">-->
                        <!---->
                        <!--                صف الموقع-->
                        <!--            </div>-->
                        <!--        </td>-->
                        <!--        <td>-->
                        <!--            <label>-->
                        <!--            </label>-->
                        <!--        </td>-->
                        <!--    </tr>-->
                        <input type="hidden" name="id" id="id" value="<?= $user->ID ?>">
                        <!--    <tr>-->
                        <!--        <td>-->
                        <!--            <div class="label">-->
                        <!---->
                        <!--                اسم المستخدم-->
                        <!--            </div>-->
                        <!--        </td>-->
                        <!--        <td>-->
                        <!--            <label>-->
                        <!--                <input type="text" id="admin_user" name="DomainName" required>-->
                        <!--            </label>-->
                        <!--        </td>-->
                        <!--    </tr>-->
                        <!--    <tr>-->
                        <!--        <td>-->
                        <!--            <div class="label">-->
                        <!---->
                        <!--                كلمة المرور-->
                        <!--            </div>-->
                        <!--        </td>-->
                        <!--        <td>-->
                        <!--            <label>-->
                        <!--                <input type="text" id="admin_pass" name="DomainName" required>-->
                        <!--            </label>-->
                        <!--        </td>-->
                        <!--    </tr>-->

                    </table>
                    <button class="submit-button-step-1" onclick="upload()">ابدأ بإعداد متجرك</button>

                </div>
            </div>

        </div>
        <!--        <div class="button__group">-->
        <!--            <button onclick="upload()">ابأ مجاناً الآن</button>-->
        <!--        </div>-->

    </div>
</div>
<?php if ($user->TrialDomain) { ?>
    <div class="accept-domain">
        <form method="post" class="TrialSiteUrl TrialSiteUrl2" action="https://<?= $user->TrialDomain ?>.rouad.tech/?admin=true"
              target="_blank">
            <input type="hidden" name="username" value=<?= $user->user_login ?>>
            <input type="hidden" name="admin" value=<?= $user->user_email ?>>
            <input type="hidden" name="password" value=<?= $user->user_pass ?>>
            <input type="hidden" name="id" value=<?= $user->ID ?>>
            <input class="TrialSiteButton " type="submit" value=" <?= __(' متابعة تجهيز الموقع', 'sso') ?>">

        </form>
    </div>
<?php }
?>
<style>


    .section-right {
        padding: 20px;

    }

    .form-success-title {
        color: #000000 !important;
        background-color: #FFFFFF;
        font-size: 30px;
    }

    .submit-button-step-1 {
        background-color: #1152F2 !important;
        border-radius: 5px !important;
        width: 230px;
        height: 44px;
        display: flex;
        margin: auto;
        align-content: center;
        align-items: center;
        justify-content: center;
    }

    .second-title-label {
        color: #2F3088 !important;
        font-size: 18px !important;
        font-weight: 600 !important;
    }

    .main-title-label {
        color: #1152F2 !important;
        font-size: 30px !important;
        font-weight: bold !important;

    }

    .Trial-hint {
        margin-top: 17px;
        text-align: center;
        color: #1152F2 !important;
        font-size: 16px !important;
        border: 1px solid;
        border-radius: 10px;
        height: 44px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 50%;
        margin: auto;
        margin-top: 27px;

    }

    .promo-Trial {
        text-align: center;
        color: #000000;
        font-size: 20px;
        margin-bottom: 27PX;
    }

    input.TrialSiteButton {
        color: #ffffff;
        border-radius: 15px;
        background-color: #1152f2;
    }

    .gap {
        height: 150px;
    }

    .space-h {
        height: 150px;
    }

    .form-input {
        background-color: #EEF7FF !important;
        border-radius: 5px !important;
    }

    table, td, th {
        border: none !important;
        padding: 5px 0px !important;
    }


    .form-label {
        color: #000000;
        font-weight: bold;
        font-size: 16px;

    }

    table#form {
        margin-top: 18px;
        width: 92%;
        max-width: 495px;
    }

    .store-name-label {
        color: #1152F2;
        font-size: 25px;
        font-weight: bold;
    }

    .store-description {
        color: #000000;
        font-size: 16px;

    }

    .section-left {
        padding: 20px;
        width: 55%;

    }

    .main-section {
        display: flex;
        justify-content: space-evenly;
        width: 80%;
        margin: auto;


    }

    .Trial-photo {
        max-width: 446px;

    }

    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600&display=swap');

 
    .body {
        background-color: #FFFFFF;

        top: 70px;
        /* width: 100%; */
        z-index: 50;
        right: 0;
        left: 0;
        /* margin: auto; */
        /*padding-top: 70px;*/
        display: flex;
        align-items: start;
        justify-content: right;
    }

    input.TrialSiteButton :hover, input.TrialSiteButton :focus {
        background-color: #1152F2 !important;

    }

    .card {
        position: relative;
        width: 100% !important;
        border-radius: 0.75rem;
    }

    thead tr th:first-child,
    tbody tr td:first-child {
        width: 13em;

        word-break: break-all;
    }

    .info__padding {
        margin-top: 70px;
    }

    h3 {
        font-size: 19px;
        margin-bottom: 12px;
        font-weight: 600;
    }

    p {
        font-size: 16px;
        font-weight: 400;
    }

    h3, p {
        color: #0B1A28;
        text-align: center;
    }
    input.TrialSiteButton:hover{
        background-color:#007AFF;
    }
    .button__group button {
        color: #007AFF;
        background-color: #fff;
        font-size: 16px;
        font-weight: 500;
        outline: none;
        border: none;
        border-top: 1px solid rgb(220, 224, 226);
        border-left: 1px solid rgb(220, 224, 226);
        padding: 18px 10px;
        cursor: pointer;
        float: left;
        width: 100%;
    }

    .button__group:after {
        content: "";
        clear: both;
        display: table;
    }

    .button__group button:not(:last-child) {
        border-left: none;
    }

    .button__group button:hover {
        background-color: rgb(250, 250, 250);
    }

    .button__group button:active {
        background-color: rgb(245, 245, 245);
    }
    @media only screen and (min-width: 992px) {
        .submit-button-step-1{
            margin-right:4px;
        }
    }
    @media only screen and (max-width: 992px) {
        .finish-progres{
            justify-content: flex-start !important;
        }
    }
    @media only screen and (max-width: 992px)and (min-width: 600px) {
        .submit-button-step-1{
            margin-right:20%;
        }
.Trial-photo{
    max-width:333px;
    margin:auto;
}
    }
    @media only screen and (min-width: 600px) {

    }

    @media only screen and (max-width: 600px) {

        .Trial-hint {
            width: 75% !important;
        }
        .submit-button-step-1{
            margin:auto !important;
        }
        .TrialSiteUrl {
            width: 100%;
            height: 100%;
        }
        .TrialSiteUrl2 {
            width: 100%;
            height: 100%;
        }
        .finish-progres {
            height: auto !important;

        }
    }


    @media only screen and (max-width: 992px) {
        .main-section {
            flex-direction: column;
        }

        .Trial-photo {
            max-width: 100%;
            width: 100%;
        }

        .section-left {
            width: 100%;
            margin-bottom: 400px;
        }

        .space-h {
            height: 50px;
        }
    }

    .finish-progres {
        border: solid 3px;
        border-radius: 24px;
        font-size: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 400px;
        margin: 10px;
        text-align: center;
        color: #ffffff;
        /* background-color: #1152F2; */
        flex-direction: column;
    }

    .label {
        display: contents;

    }

    li {
        margin: 10px;
    }

    .accept-domain {
        border: solid 3px;
        border-radius: 24px;
        font-size: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 50%;
        margin: 10px;
        color: #ffffff;
        margin: auto;
    }

    #content {
        min-height: 500px;
    }

    .TrialSiteUrl2 {
        width: 50%%;
        height: 100%;
    }

    .field-domain {
        border: solid 3px;
        border-radius: 24px;
        font-size: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 75px;
        margin: 10px;
        color: #ffffff;
        background-color: #1152F2;
    }

</style>

<script>

    jQuery(window).load(function () {
        setTimeout(function () {
            jQuery.ajax({
                type: "post",
                url: "../wp-admin/admin-ajax.php",
                data: {
                    action: 'createData',
                },
                success: function (html) {

                    jQuery.ajax({
                        type: "post",
                        url: "../wp-admin/admin-ajax.php",
                        data: {
                            action: 'setup_1',
                        },
                        success: function (html) {
                            //   jQuery(".submit-button-step-1").show();

                            jQuery.ajax({
                                type: "post",
                                url: "../wp-admin/admin-ajax.php",
                                data: {
                                    action: 'setup_2',
                                },
                                success: function (html) {


                                }
                            });


                        }
                    });


                }
            });
            jQuery.ajax({
                type: "post",
                url: "../wp-admin/admin-ajax.php",
                data: {
                    action: 'installProjectFile',
                },
                success: function (html) {

                }
            });

        }, 1000);

    });

    function save_details() {
        jQuery(".body").hide();

    }

    jQuery(".finish-progres").hide();
    // jQuery(".submit-button-step-1").hide();
    var i = 0;

    function progres() {
        if (i == 0) {
            i = 1;
            var elem = document.getElementById("myBar");
            var width = 1;
            var id = setInterval(frame, 200);

            function frame() {
                if (width >= 98) {
                    clearInterval(id);
                    i = 0;
                } else {

                    width++;
                    elem.style.width = width + "%";
                }
            }
        }
    }

    //       jQuery(".modal-body").show();
    //     event.preventDefault()
    function upload() {
        // code here
        siteName = jQuery("#siteName").val();
        TrialDomain = jQuery("#TrialDomain").val();

        //  description = jQuery("#description").val();
        description = jQuery("#siteName").val();
        id = jQuery("#id").val();


        var letters = /^[A-Za-z\s]+$/;
        if (TrialDomain.match(letters)) {
            jQuery(".submit-button-step-1").hide("slow");
            jQuery(".store-name-label").hide("slow");
            jQuery(".store-description").hide("slow");

            //  alert("الرجاء ادخال اسم امتجر باللغة العرية");
            TrialDomain = TrialDomain.replace(" ", "-");
            //  alert(TrialDomain);

            // return false;


            jQuery("#myProgress").show("slow");

            jQuery("#form").hide()
            progres();
            jQuery.ajax({
                type: "post",
                url: "../wp-admin/admin-ajax.php",
                data: {
                    action: 'installTrial',
                    TrialDomain: TrialDomain,
                    siteName: siteName,
                    description: description,
                    id: id
                },
                success: function (html) {
                    jQuery(".TrialSiteUrl").attr("action", "https://" + html + ".rouad.tech")
                    jQuery(".TrialSiteButton").attr("value", "اضغط للدول إلى" + html)
                    jQuery(".finish-progres").show()

                    jQuery("#myProgress").hide("slow");
                    jQuery(".promo-Trial").hide("slow");
                    jQuery(".TrialSiteUrl").submit();


                }
            });
        } else {
            alert("الرجاء ادخال اسم المتجر باللغة لانكليزية");
            return false;
        }

    };

</script>
<style>
    table, td, th {
        padding: 5px 0px !important;

    }

    #myProgress {
        width: 100%;
        background-color: #d0cece;

        display: none
    }

    #myBar {
        width: 1%;
        height: 30px;
        background-color: #1152F2;
    }

    input {
        width: 100%;

    }

    .table-result {
        width: 100%;

    }

    .table-result tr th {
        text-align: center;
    }

    .table-result tr td {
        padding: 10px;

        text-align: center;
    }

    .reserv-button {
        margin: 15px;

        border: none;
        background-color: #ffdd22;
        padding: 10px;
        border-radius: 5px;
    }

    .submit-button {

        height: 50px;
        width: 140px;

    }

    .DomainName {
        height: 50px;
    }

    .container-div {
        display: flex;
        /* justify-content: center; */
        margin-top: 30px;
    }

    .table-result {
        /*display: none;*/
    }

    .modal-add {
        position: relative;
    }

    .modal-body {
        position: absolute;
        display: flex;
        width: 100%;
        top: 0;
        right: 0%;
        left: 0px;
        background-color: #FFFFFF;
        justify-content: center;
    }


    .error-message {
        display: flex;
        justify-content: center;
        background-color: crimson;
        padding: 10px;
        color: #FFFFFF;
        margin-bottom: 15px;
        border-radius: 10px;
        max-width: 500px;
        margin: auto;
    }

    .modal-add {
        position: relative;
    }

    .modal-body {
        display: none;
        position: absolute;
        width: 1000px;
        top: 0;
        right: 0px;
        /* left: 0px; */
        background-color: #FFFFFF;
    }

    /* Center the svg and set the background to white */


    /* Make the svg a white circle and give it the default spinning animation */
    #svg {
        background-color: #ffffff;
        border-radius: 50%;
        animation: spin 3s ease infinite alternate;
    }

    /*Give each dot a radius of 20*/
    .shape {
        r: 20;
    }

    /*Give each dot its positioning and set the default animation and color for each */
    .shape:nth-child(1) {
        cy: 50;
        cx: 50;
        fill: #c20f00;
        animation: movein 3s ease infinite alternate;
    }

    .shape:nth-child(2) {
        cy: 50;
        cx: 150;
        fill: #ffdd22;
        animation: movein 3s ease infinite alternate;
    }

    .shape:nth-child(3) {
        cy: 150;
        cx: 50;
        fill: #2374c6;
        animation: movein 3s ease infinite alternate;
    }

    .shape:nth-child(4) {
        cy: 150;
        cx: 150;
        fill: #ffdd22;

        animation: movein 3s ease infinite alternate;
    }

    /* Put the two interface options at the bottom of the screen */
    .control-panel {
        position: fixed;
        bottom: 5px;
        display: flex;
        align-items: center;
    }

    /* Set color and placement of labels */
    .switch-label {
        display: inline-block;
        fill: #ffdd22;
        margin: 5px;
    }

    /* Set area of switches */
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 25px;
    }

    /* Get rid of checkbox defaults */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* Create the slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ffdd22;
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 19px;
        width: 19px;
        left: 3px;
        bottom: 3px;
        background-color: #000000;
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }

    /* Change color on checked */
    input:checked + .slider {
        background-color: #c20f00;
    }

    /* Set second color change */
    .movement input:checked + .slider {
        fill: #ffdd22;
    }

    /* Set the focus to same color as when checked*/
    input:focus + .slider {
        box-shadow: 0 0 3px #c20f00;
    }

    .movement input:focus + .slider {
        fill: #ffdd22;
    }

    /* Actually move the slider when checked*/
    input:checked + .slider:before {
        -webkit-transform: translateX(25px);
        -ms-transform: translateX(25px);
        transform: translateX(25px);
    }

    /* Round the sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    /*Spin the svg so all the dots spin*/
    @keyframes spin {
        to {
            transform: rotate(1turn);
        }
    }

    /* Move all the dots toward the center */
    @keyframes movein {
        to {
            cy: 100;
            cx: 100;
        }
    }

    /* Set a bouncy ball type movement for the dots */
    @keyframes moveup {
        to {
            cy: 20;
        }
    }

</style>