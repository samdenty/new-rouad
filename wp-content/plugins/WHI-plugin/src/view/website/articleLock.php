<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/bootstrap.min.css' ?>" rel="stylesheet" id="bootstrap-css">
<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/style.css' ?>" rel="stylesheet">
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/jquery.min.js' ?>"></script>
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/bootstrap.min.js' ?>"></script>
<?php if ($widget->style) { ?>
    <style>
        <?=$widget->style;?>
    </style>
<?php } ?>
<div id="sogMembershipModal" class="" data-backdrop="static" data-keyboard="false">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header" style="text-align: center; padding: 10px;">
            <p style="font-weight: bold; display: contents; color: #E96552;">انتهى رصيدك</p>
        </div>
        <div class="modal-body" style="text-align: center;
        <?php if ($widget->background) { ?>background:<?= $widget->background ?>!important;"<?php } ?>">
        <p style="<?php if ($widget->fontsize) { ?>font-size:<?= $widget->fontsize ?>!important;<?php } ?>
        <?php if ($widget->fontcolor) { ?>color:<?= $widget->fontcolor ?>!important;<?php } ?>">
            لقد استهلكت المقالات المتاح لك قراءتها ضمن الاشتراك الأساسي.. <a href="<?php echo site_url(); ?>">العودة
                للرئيسية</a></p>
    </div>
</div>
</div>
