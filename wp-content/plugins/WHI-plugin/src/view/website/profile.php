<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/bootstrap.min.css' ?>" rel="stylesheet" id="bootstrap-css">
<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/style.css' ?>" rel="stylesheet">
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/jquery.min.js' ?>"></script>
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/bootstrap.min.js' ?>"></script>
<?php $user_icon = plugin_dir_url(__FILE__) . 'assets/icons/user.svg'; ?>
<?php $logout_icon = plugin_dir_url(__FILE__) . 'assets/icons/logout.svg'; ?>
<?php if ($widget->style) { ?>
    <style>
        <?=$widget->style;?>
    </style>
<?php } ?>
<div class="container" id="form">
    <ul class="navbar-nav mr-auto login-list">
        <li class="nav-item shopping-cart">
            <a href="#" class="nav-link" id="shopping-cart-btn">
                <span class="badge">0</span></a>
        </li>
        <li id="user" class="nav-item user-account">
            <a href="#" class="user-account-btn nuxt-link-exact-active nuxt-link-active" aria-current="page">
                <section>
                    <span class="icon"
                          style="background: url(<?= $user_icon ?>) 0 0 no-repeat padding-box !important;background-size: 100%  100% !important"></span>
                    <span class="text">Owner </span>
                </section>
                <div class="control-dropdown dropdown postion_dropdown_remove" id="drop_down_user">
                    <div class="triangle-up triangle-up_2"></div>
                    <ul class="dropdown">
                        <li class="item account"
                            <?php if ($widget->background) { ?>
                                style="background:<?= $widget->background ?>!important;"
                            <?php } ?>>
                            <a href="/my-account" class="link">
                                <span class="text" <?php if ($widget->fontsize) { ?>
                                    style="font-size:<?= $widget->fontsize ?>!important;"<?php } ?> >الصفحة الشخصية</span></a>
                        </li>
                        <li class="item subscription">
                            <a href="/my-account/subscriptions" class="link">
                                <span class="text"
                                      style="<?php if ($widget->fontsize) { ?>font-size:<?= $widget->fontsize ?>!important;<?php }
                                      if ($widget->fontcolor) { ?>;color: <?=$widget->fontcolor?><?php } ?>">اشتراكاتي</span>
                            </a>
                        </li>
                        <li data-v-31feb1af="" class="item orders">
                            <a href="/my-account/orders" class="link">
                                <span class="text"
                                      style="<?php if ($widget->fontsize) { ?>font-size:<?= $widget->fontsize ?>!important;<?php }
                                      if ($widget->fontcolor) { ?>;color: <?=$widget->fontcolor?><?php } ?>">طلباتي</span>
                            </a>
                        </li>
                        <li data-v-31feb1af="" class="item addresses">
                            <a href="/my-account/addresses" class="link">
                                <span class="text"
                                      style="<?php if ($widget->fontsize) { ?>font-size:<?= $widget->fontsize ?>!important;<?php }
                                      if ($widget->fontcolor) { ?>;color: <?=$widget->fontcolor?><?php } ?>">عناويني</span>
                            </a>
                        </li>
                        <li class="item logout">
                            <a href="/#" class="link nuxt-link-exact-active nuxt-link-active" aria-current="page">
                                <span class="icon"
                                      style="background: url(<?= $logout_icon ?>) 0 0 no-repeat padding-box !important;"></span>
                                <span class="text"
                                      <?php if ($widget->fontsize) { ?>
                                          style="font-size:<?= $widget->fontsize ?>!important;"<?php } ?>>تسجيل الخروج</span></a>
                        </li>
                    </ul>
                </div>
            </a>
        </li>
    </ul>
</div>

<script>
    $(document).ready(function () {
        $("#user").click(function () {
            if ($("#drop_down_user").hasClass("postion_dropdown_remove")) {
                $("#drop_down_user").removeClass("postion_dropdown_remove");
                $("#drop_down_user").addClass("postion_dropdown");
            } else {
                $("#drop_down_user").removeClass("postion_dropdown");
                $("#drop_down_user").addClass("postion_dropdown_remove");
            }
        });
    });
</script>