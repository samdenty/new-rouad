<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/bootstrap.min.css' ?>" rel="stylesheet" id="bootstrap-css">
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/bootstrap.min.js' ?>"></script>
<script src="<?= plugin_dir_url(__FILE__) . 'assets/js/jquery.min.js' ?>"></script>
<link href="<?= plugin_dir_url(__FILE__) . 'assets/css/style.css' ?>" rel="stylesheet">
<?php $icon = plugin_dir_url(__FILE__) . 'assets/icons/user_login.svg'; ?>
<?php if ($widget->style) { ?>
    <style>
        <?=$widget->style;?>
    </style>
<?php } ?>
<div class="container" id="form">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item login">
            <a href="/login" class="login-btn"
                <?php if ($widget->background) { ?> style="<?= $widget->background; ?> !important" <?php } ?>>
                <span class="text text_login"
                      style="<?php if ($widget->fontsize) { ?> font-size:<?= $widget->fontsize; ?> !important <?php }
                      if ($widget->fontcolor) { ?>;color:<?=$widget->fontcolor; ?> !important; <?php } ?>">تسجيل الدخول</span>
                <span style="background: url('<?= $icon ?>') 0 0 no-repeat padding-box" class="icon"></span>
            </a>
        </li>
        <li class="nav-item account">
            <a href="/signup" class="nav-link nav_link_sigup" id="create-account-btn">إنشاء حساب</a>
        </li>
    </ul>
</div>