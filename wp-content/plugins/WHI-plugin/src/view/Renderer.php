<?php

namespace Sso\WP\View;

use Sso\WP\Helpers\FunctionApiHelper;
use Sso\WP\Helpers\AuthenticatHelper;
use Sso\WP\Helpers\SettingHelper;

class Renderer
{
    private static $instance;
    const ADMIN_VIEWS = __DIR__ . '/admin';
    const WEBSITE_VIEWS = __DIR__ . '/website';

    private function __construct()
    {
        $this->settings = SettingHelper::getInstance();
        $this->auth = AuthenticatHelper::getInstance();
        $this->api = FunctionApiHelper::getInstance();
        /* instal widget */
        $widget = $this->settings->getNameWidgets();
        foreach ($widget as $w) {
            $this->settings->setWidgets($w);
        }
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Renderer();
        }
        return self::$instance;
    }

    public function createAdminMenu()
    {


        add_menu_page(
            __( 'home WHI', 'WHI'),
            __('WHI', 'WHI'),
            'publish_posts',
            'WHI',
            [$this, 'renderIndexPage'],
            $this->getAdminAssetsUrlIcon() . '/menu-icon.png',
            146000
        );

        add_submenu_page(
            'WHI',
            __('WHI setting ', 'WHI'),
            __('Setting', 'WHI'),
            'publish_posts',
            'setting',
            [$this, 'renderSettingPage'],
            $this->getAdminAssetsUrlIcon() . '/setting-icon.png',
            146001
        );

    //    if ($this->settings->checkAuth() ) {
//
//            add_submenu_page(
//                'WHI',
//                __('Widget ', 'WHI'),
//                __('Widget', 'WHI'),
//                'publish_posts',
//                'widget',
//                [$this, 'renderWidgetPage'],
//                $this->getAdminAssetsUrlIcon() . '/setting-icon.png',
//                146002
//            );

            add_submenu_page(
                'WHI',
                __('Subscriber ', 'WHI'),
                __('Subscriber', 'WHI'),
                'publish_posts',
                'subscriber',
                [$this, 'renderSubscriberPage'],
                $this->getAdminAssetsUrlIcon() . '/setting-icon.png',
                146003
            );

//            add_submenu_page(
//                'sso',
//                __('Document ', 'sso'),
//                __('Document', 'sso'),
//                'publish_posts',
//                'document',
//                [$this, 'renderDocumentPage'],
//                $this->getAdminAssetsUrlIcon() . '/setting-icon.png',
//                146004
//            );

      //  }


    }


    public function enqueueAdminScripts()
    {
        wp_enqueue_style(
            'sso-admin-css',
            $this->getAdminAssetsUrl() . '/css/style.css',
            [],
            WHI_VERSION,
            null
        );

        wp_enqueue_style(
            'sso-admin-bootstrap-css',
            $this->getAdminAssetsUrl() . '/css/bootstrap.min.css',
            [],
            WHI_VERSION,
            null
        );

        wp_enqueue_script(
            'sso-admin-js',
            $this->getAdminAssetsUrl() . '/js/script.js',
            [],
            WHI_VERSION,
            null
        );

        wp_enqueue_script(
            'sso-admin-jquery',
            $this->getAdminAssetsUrl() . '/js/jquery.min.js',
            [],
            WHI_VERSION,
            null
        );


        wp_enqueue_script(
            'sso-admin-bootstrap-js',
            $this->getAdminAssetsUrl() . '/js/bootstrap.min.js',
            [],
            WHI_VERSION,
            null
        );

    }

    public function getAdminAssetsUrl()
    {
        return plugin_dir_url(__FILE__) . 'admin/assets';
    }

    public function getAdminAssetsUrlIcon()
    {
        return plugin_dir_url(__FILE__) . 'admin/assets/icons';
    }

    public function renderSubscriberPage()
    {
        /*
        ** statment for all authentication
        */

        $this->setSubscriberPage();
    }


    public function renderDocumentPage()
    {
        /*
        ** statment for all authentication
        */

        $this->setDocumentPage();
    }

    public function renderWidgetPage()
    {
        /*
        ** statment for all authentication
        */

        $this->setWidgetsPage();
    }

    public function renderSettingPage()
    {
        /*
        ** statment for all authentication
        */

        $this->setSettingPage();
    }


    public function renderIndexPage()
    {
        /*
        ** statment for all authentication
        */
        $this->setIndexPage();
    }

    public function setSettingPage()
    {
        require_once self::ADMIN_VIEWS . '/setting.php';
    }

    public function setIndexPage()
    {
        require_once self::ADMIN_VIEWS . '/index.php';
    }

    public function setWidgetsPage(){

        if(isset($_POST["edit_widget"])){
            $this->saveWidget();
        }elseif(isset($_GET["id"]) && isset($_GET["name"])){
            $widgetId=$_GET["id"];
            $widgetName=$_GET["name"];
            require_once self::ADMIN_VIEWS . '/edit-widget.php';
        }else{
            $data=$this->settings->getWidgets();
            require_once self::ADMIN_VIEWS . '/widget.php';
        }
    }

    private function saveWidget(){
        global $wpdb;
        $table = "hbrar_options";
        $widgetName=$_GET["name"];
        $widgetId=$_GET["id"];
        $option=get_option($widgetName);
        $data=json_decode($option);
        $data->background=$_POST['background'];
        $data->style=$_POST['style'];
        $data->fontsize=$_POST['fontsize'];
        $data->fontcolor=$_POST['fontcolor'];
        $value=wp_json_encode($data);
        $result=$wpdb->update($table,  array("option_value" => $value), array('option_id'=>$widgetId));
        if($result!=0) {
            $url = '/new-hbrarabic/wp-admin/admin.php?page=widget';
            wp_redirect($url);
        }else{
            echo "not save changed";
            exit();
        }
    }

    public function setSubscriberPage()
    {
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $info_person = $this->api->getMemberById($id);
            require_once self::ADMIN_VIEWS . '/show-subsciber.php';
        } else {
            if (isset($_GET["section"])) $page_active = $_GET["section"];
            else  $page_active = 1;
            $info = $this->api->getMembersInfo($page_active);
            require_once self::ADMIN_VIEWS . '/subscriber.php';
        }
    }

    public function setDocumentPage(){
        require_once self::ADMIN_VIEWS . '/document.php';
    }

    public function setSigninPage()
    {
        $widget=json_decode(get_option("Widget-Signin"));
        require_once self::WEBSITE_VIEWS . '/signin.php';
    }

    public function setProfilePage()
    {
        $widget=json_decode(get_option("Widget-Profile-subscriber"));
        require_once self::WEBSITE_VIEWS . '/profile.php';
    }

    public function setArticleLockPage()
    {
        $widget=json_decode(get_option("Widget-ArticleLock"));
        require_once self::WEBSITE_VIEWS . '/articleLock.php';
    }

    public function AddDomainWidget()
    {
     //   $widget=json_decode(get_option("Widget-ArticleLock"));
        require_once self::WEBSITE_VIEWS . '/AddDomain.php';
    }


    public function AddDomainAcceptWidget()
    {
       // $widget=json_decode(get_option("Widget-ArticleLock"));
        require_once self::WEBSITE_VIEWS . '/AcceptDomain.php';
    }
    public function TrialHostWidget()
    {
        // $widget=json_decode(get_option("Widget-ArticleLock"));
        require_once self::WEBSITE_VIEWS . '/InitTrial.php';
    }
}