# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <a.alhussni@haykalmedia.com>, YEAR.

#all menu title
msgid "Sso"
msgstr ""

msgid "home sso"
msgstr ""

msgid "sso setting"
msgstr ""

msgid "Setting"
msgstr ""

msgid "Widget"
msgstr ""

msgid "Subscriber"
msgstr ""

msgid "Document"
msgstr ""
msgid "Add Domain"
msgstr"Add  Domain"
#page sso
msgid "About sso plugin"
msgstr ""

msgid "your order is accepted"
msgstr "your order is accepted"

msgid "This plugin helps to communicate with the SSO system by sending and receiving information from the SSO system and performs the following functions:<br>"
msgstr ""

msgid "-Create a widget to be used on more than one site and manage its entire design by the administrator<br>"
msgstr ""

msgid "-Display an interface that helps to adjust the settings of the tool<br>"
msgstr ""

msgid "-View subscriber information in detail<br>"
msgstr ""

msgid "-Add a set of filters that manage operations and communicate with SSO<br>"
msgstr ""

msgid "-Description of all filters used<br>"
msgstr ""

msgid "go to"
msgstr ""

#page setting
msgid "purchase key"
msgstr ""

msgid "Enter purchase key"
msgstr ""

#page widget
msgid "show widget"
msgstr ""

msgid "title"
msgstr ""

msgid "edit"
msgstr ""

#page edit-widget
msgid "Edit"
msgstr "تعديل"

msgid "background"
msgstr ""

msgid "background"
msgstr ""

msgid "font size"
msgstr ""

msgid "font color"
msgstr ""

msgid "style"
msgstr ""

msgid "Submit"
msgstr ""

#page subscriber
msgid "show subscriber"
msgstr ""

msgid "username"
msgstr ""

msgid "email"
msgstr ""

msgid "first name"
msgstr ""

msgid "last name"
msgstr ""

msgid "show"
msgstr ""

msgid "select page"
msgstr ""

#page show-subscriber
msgid "Information"
msgstr ""

msgid "membership_plans"
msgstr ""

msgid "access duration"
msgstr ""

msgid "access num"
msgstr ""

msgid "access period"
msgstr ""

msgid "access start date"
msgstr ""

msgid "access end date"
msgstr ""

msgid "paid content"
msgstr ""

#page documention
msgid "functions documention"
msgstr ""

msgid "widget documention"
msgstr ""

msgid "filter name"
msgstr ""

msgid "arguments"
msgstr ""

msgid "return"
msgstr ""

msgid "description"
msgstr ""

