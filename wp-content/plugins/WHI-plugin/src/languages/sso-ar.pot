# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <a.alhussni@haykalmedia.com>, YEAR.

#all menu title
msgid "Sso"
msgstr ""

msgid "home sso"
msgstr "الصفحة الرئيسية"

msgid "sso setting"
msgstr "صفحة الإعدادات"

msgid "Setting"
msgstr "الإعدادت الرئيسية"

msgid "Widget"
msgstr "تصاميم"

msgid "Subscriber"
msgstr "المشتركين"

msgid "Document"
msgstr "التوصيف"

#page sso
msgid "About sso plugin"
msgstr "حول البرنامج المساعد sso"

msgid "This plugin helps to communicate with the SSO system by sending and receiving information from the SSO system and performs the following functions:<br>"
msgstr "يساعد هذا المكون الإضافي على التواصل مع نظام SSO عن طريق إرسال واستقبال المعلومات من نظام SSO ويقوم بالوظائف التالية: <br>"

msgid "-Create a widget to be used on more than one site and manage its entire design by the administrator<br>"
msgstr "-إنشاء عنصر واجهة مستخدم لاستخدامه على أكثر من موقع وإدارة تصميمه بالكامل بواسطة المسؤول <br>"

msgid "-Display an interface that helps to adjust the settings of the tool<br>"
msgstr "-عرض واجهة تساعد على ضبط إعدادات الأداة <br>"

msgid "-View subscriber information in detail<br>"
msgstr "-عرض معلومات المشترك بالتفصيل <br>"

msgid "-Add a set of filters that manage operations and communicate with SSO<br>"
msgstr "-إضافة مجموعة من الفلاتر التي تدير العمليات وتتواصل مع SSO <br>"

msgid "-Description of all filters used<br>"
msgstr "-وصف جميع الفلاتر المستخدمة"

msgid "go to"
msgstr "الذهاب إلى"

#page setting
msgid "purchase key"
msgstr "مفتاح الدخول"

msgid "Enter purchase key"
msgstr "إدخل مفتاح الدخول"

#page widget
msgid "show widget"
msgstr "عرض جميع التصاميم"

msgid "title"
msgstr "عنوان"

msgid "edit"
msgstr "تعديل"

msgid "Edit"
msgstr "تعديل"

msgid "background"
msgstr "الخلفية"

msgid "font size"
msgstr "حجم الخط"

msgid "font color"
msgstr "لون الخط"

msgid "style"
msgstr "إضافة تصميم"

msgid "Submit"
msgstr "إرسال"

#page subscriber
msgid "show subscriber"
msgstr "عرض المشتركين"

msgid "username"
msgstr "اسم المشترك"

msgid "your order is accepted"

msgstr "تم قبول طلبك "

msgid "email"
msgstr "البريدالإلكتروني"

msgid "first name"
msgstr "الاسم الاول"

msgid "last name"
msgstr "الاسم الثاني"

msgid "show"
msgstr "عرض"

msgid "Add Domain"
msgstr "اضافة نطاق "

msgid "select page"
msgstr "تحديد الصفحة"

#page show-subscriber
msgid "Information"
msgstr "معلومات المشترك"

msgid "membership plans"
msgstr "العضويات"

msgid "access duration"
msgstr "مدة الوصول"

msgid "access num"
msgstr "عدد مرات الوصول"

msgid "access period"
msgstr "فترة الوصول"

msgid "access start date"
msgstr "تاريخ بدء الوصول"

msgid "access end date"
msgstr "تاريخ نهاية الوصول"

msgid "paid content"
msgstr "المحتوى المدفوع"

#page documention
msgid "functions documention"
msgstr "توصيف التوابع"

msgid "widget documention"
msgstr "توصيف التصاميم"

msgid "filter name"
msgstr "اسم الفلتر"

msgid "arguments"
msgstr "الوسطاء"

msgid "return"
msgstr "النتيجة"

msgid "description"
msgstr "الوصف"


